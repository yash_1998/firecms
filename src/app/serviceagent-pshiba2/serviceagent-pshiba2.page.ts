// import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
// import { ToastController } from '@ionic/angular';
// import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
// const { Camera, Geolocation } = Plugins;
// import Cropper from 'cropperjs/dist/cropper.esm.js';

// @Component({
//   selector: 'app-serviceagent-pshiba2',
//   templateUrl: './serviceagent-pshiba2.page.html',
//   styleUrls: ['./serviceagent-pshiba2.page.scss'],
// })
// export class ServiceagentPshiba2Page implements OnInit {
//   locationObj : any;error='';counter=0;isSaved= false;image:any;cropImage:any;

//   @ViewChild('filePicker', { static: false }) filePickerRef: ElementRef<HTMLInputElement>;

//   constructor(public toastController:ToastController) {
//     this.getCurrentPosition();
//    }

//   ngOnInit() {
//   }

//   async getCurrentPosition() {
//     try {
//       const coordinates = await Geolocation.getCurrentPosition();
//       console.log('Current', coordinates);
//       this.locationObj = coordinates.coords;
//     } catch (e) {
//       this.error = JSON.stringify(e);
//       if (this.counter <= 4) {
//         this.getCurrentPosition();
//         setTimeout(async () => {
//           const toast = await this.toastController.create({
//             message: 'Getting GPS location: Retry ' + this.counter,
//             duration: 2000
//           });
//           toast.present();
//           this.counter++;
//         }, 1000);
//       }
//     }
//   }

//   async getPicture(type: string) {
//     this.isSaved = false;
//     if (type === 'gallery') {
//       this.filePickerRef.nativeElement.click();
//       return;
//     }
//     const image = await Camera.getPhoto({
//       quality: 100,
//       resultType: CameraResultType.Base64,
//       source: CameraSource.Camera
//     });
//     var imageUrl = 'data:image/jpeg;base64,' + image.base64String;
//     this.image = imageUrl;
//     console.log('Image base64 => ', imageUrl);
//     setTimeout(() => {
//       this.bindWithCropper();
//     }, 1000);
//   }

//   bindWithCropper() {
//     if (this.cropImage) {
//       this.cropImage.destroy();
//     }
//     const image = document.getElementById('image');
//     this.cropImage = new Cropper(image, {
//       autoCropArea: 0.5,
//       responsive: true,
//       center: true,
//       crop(event) {
//         // console.log(event.detail.x);
//         // console.log(event.detail.y);
//       },
//     });
//   }

//   onFileChoose(event: Event) {
//     const file = (event.target as HTMLInputElement).files[0];
//     const pattern = /image-*/;
//     const reader = new FileReader();

//     if (!file.type.match(pattern)) {
//       console.log('File format not supported');
//       return;
//     }

//     reader.onload = () => {
//       this.image = reader.result.toString();
//       console.log('Image base64 => ', this.image);

//       setTimeout(() => {
//         this.bindWithCropper();
//       }, 1000);
//     };
//     reader.readAsDataURL(file);
//   }

//   async crop() {
//     console.log('Image save => ', this.cropImage.getCroppedCanvas().toDataURL());
//     this.isSaved = true;
//     this.image = await this.cropImage.getCroppedCanvas().toDataURL();
//   }
// }























// ===============================================================================================================================
import { Component, ElementRef, ViewChild } from '@angular/core';
import { Platform, ToastController } from '@ionic/angular';
import { Plugins, CameraResultType, CameraSource } from '@capacitor/core';
import Cropper from 'cropperjs/dist/cropper.esm.js';
import { CommonService } from '../common.service';

const { Camera, Geolocation } = Plugins;

@Component({
  selector: 'app-android-home',
  templateUrl: './serviceagent-pshiba2.page.html',
  styleUrls: ['./serviceagent-pshiba2.page.scss'],
})
export class ServiceagentPshiba2Page {

  @ViewChild('filePicker', { static: false }) filePickerRef: ElementRef<HTMLInputElement>;

  image: any;
  cropImage: any;
  isSaved: any = false;
  locationObj: any;
  error: any;
  counter: any = 1;
  cam: any = true;

  constructor(
    public platform: Platform,
    public common: CommonService
  ) {
    this.getCurrentPosition();
  }

  async getCurrentPosition() {
    try {
      const coordinates = await Geolocation.getCurrentPosition();
      console.log('Current', coordinates);
      this.locationObj = coordinates.coords;
      this.error = '';
    } catch (e) {
      this.error = JSON.stringify(e);
      console.log('Error => ', e);

      if (e?.message == "location unavailable") {
        this.common.showActionAlert('Didn\'t Find your location. Please turn on your location or set it to high accurancy').then(data => {
          if (data) {
            this.getCurrentPosition();
          }
        });

        return;
      }
      if (this.counter < 5) {
        setTimeout(async () => {
          this.common.showToast(this.counter)
          this.counter++;
          this.getCurrentPosition();
        }, 3000);
      }
    }
  }

  async getPicture(type: string) {
    this.isSaved = false;
    if (type === 'gallery') {
      this.filePickerRef.nativeElement.click();
      return;
    }
    const image = await Camera.getPhoto({
      quality: 100,
      resultType: CameraResultType.Base64,
      source: CameraSource.Camera
    });
    var imageUrl = 'data:image/jpeg;base64,' + image.base64String;
    this.image = imageUrl;
    console.log('Image base64 => ', imageUrl);
    setTimeout(() => {
      this.bindWithCropper();
    }, 1000);
  }

  bindWithCropper() {
    
    if (this.cropImage) {
      this.cropImage.destroy();
    }
    const image = document.getElementById('image');
    this.cropImage = new Cropper(image, {
      autoCropArea: 0.5,
      responsive: true,
      center: true,
      crop(event) {
        // console.log(event.detail.x);
        // console.log(event.detail.y);
      },
    });
  }

  async crop() {
    console.log('Image save => ', this.cropImage.getCroppedCanvas().toDataURL());
    this.isSaved = true;
    this.image = await this.cropImage.getCroppedCanvas().toDataURL();
  }

  onFileChoose(event: Event) {
    const file = (event.target as HTMLInputElement).files[0];
    const pattern = /image-*/;
    const reader = new FileReader();

    if (!file.type.match(pattern)) {
      console.log('File format not supported');
      return;
    }

    reader.onload = () => {
      this.image = reader.result.toString();
      console.log('Image base64 => ', this.image);

      setTimeout(() => {
        this.bindWithCropper();
      }, 1000);
    };
    reader.readAsDataURL(file);
  }

  reset() {
    this.isSaved = true;
    this.image = '';
  }
}



