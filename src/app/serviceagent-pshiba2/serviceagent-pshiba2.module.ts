import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ServiceagentPshiba2PageRoutingModule } from './serviceagent-pshiba2-routing.module';

import { ServiceagentPshiba2Page } from './serviceagent-pshiba2.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ServiceagentPshiba2PageRoutingModule
  ],
  declarations: [ServiceagentPshiba2Page]
})
export class ServiceagentPshiba2PageModule {}
