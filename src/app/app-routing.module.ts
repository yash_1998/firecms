import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  { path: 'register', loadChildren: './register/register.module#RegisterPageModule' },
  { path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardPageModule' },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then(m => m.LoginPageModule)
  },
  {
    path: 'contact',
    loadChildren: () => import('./contact/contact.module').then(m => m.ContactPageModule)
  },
  {
    path: 'dashboard',
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardPageModule)
  },
  {
    path: 'folder/:id',
    loadChildren: () => import('./folder/folder.module').then(m => m.FolderPageModule)
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then(m => m.HomePageModule)
  },
  // {
  //   path: 'logs',
  //   loadChildren: () => import('./logs/logs.module').then( m => m.LogsPageModule)
  // },
  // {
  //   path: 'dial',
  //   loadChildren: () => import('./dial/dial.module').then( m => m.DialPageModule)
  // },
  // {
  //   path: 'contact',
  //   loadChildren: () => import('./contact/contact.module').then( m => m.ContactPageModule)
  // },
  // {
  //   path: 'calllogs',
  //   loadChildren: () => import('./modals/calllogs/calllogs.module').then( m => m.CalllogsPageModule)
  // },
  // {
  //   path: 'viewcalllogs',
  //   loadChildren: () => import('./viewcalllogs/viewcalllogs.module').then( m => m.ViewcalllogsPageModule)
  // },
  // {
  //   path: 'gscan',
  //   loadChildren: () => import('./gscan/gscan.module').then( m => m.GscanPageModule)
  // },
  // {
  //   path: 'scan',
  //   loadChildren: () => import('./scan/scan.module').then( m => m.ScanPageModule)
  // },
  // {
  //   path: 'serviceagententry',
  //   loadChildren: () => import('./serviceagententry/serviceagententry.module').then( m => m.ServiceagententryPageModule)
  // },
  // {
  //   path: 'serviceagent-pshiba',
  //   loadChildren: () => import('./serviceagent-pshiba/serviceagent-pshiba.module').then( m => m.ServiceagentPshibaPageModule)
  // },
  {
    path: 'serviceagent-pshiba2',
    loadChildren: () => import('./serviceagent-pshiba2/serviceagent-pshiba2.module').then(m => m.ServiceagentPshiba2PageModule)
  },
  {
    path: 'whatsagent',
    loadChildren: () => import('./whatsagent/whatsagent.module').then(m => m.WhatsagentPageModule)
  },
  {
    path: 'crud',
    loadChildren: () => import('./crud/crud.module').then(m => m.CrudPageModule)
  },
  {
    path: 'products',
    loadChildren: () => import('./products/products.module').then(m => m.ProductsPageModule)
  },
  {
    path: 'home-management',
    loadChildren: () => import('./home-management/home-management.module').then(m => m.HomeManagementPageModule)
  },
  {
    path: 'add-product',
    loadChildren: () => import('./add-product/add-product.module').then(m => m.AddProductPageModule)
  },
  {
    path: 'edit-product/:id',
    loadChildren: () => import('./add-product/add-product.module').then(m => m.AddProductPageModule)
  },
  {
    path: 'product-detail/:id',
    loadChildren: () => import('./product-detail/product-detail.module').then(m => m.ProductDetailPageModule)
  },
  {
    path: 'add-feature',
    loadChildren: () => import('./add-feature/add-feature.module').then(m => m.AddFeaturePageModule)
  },
  {
    path: 'add-feature/:id',
    loadChildren: () => import('./add-feature/add-feature.module').then(m => m.AddFeaturePageModule)
  },
  {
    path: 'edit-feature/:id/:index',
    loadChildren: () => import('./add-feature/add-feature.module').then(m => m.AddFeaturePageModule)
  },
  {
    path: 'company-setting',
    loadChildren: () => import('./company-setting/company-setting.module').then(m => m.CompanySettingPageModule)
  },
  {
    path: 'product-management',
    loadChildren: () => import('./product-management/product-management.module').then(m => m.ProductManagementPageModule)
  },
  {
    path: 'add-price',
    loadChildren: () => import('./add-price/add-price.module').then(m => m.AddPricePageModule)
  },
  {
    path: 'add-price/:id',
    loadChildren: () => import('./add-price/add-price.module').then(m => m.AddPricePageModule)
  },
  {
    path: 'edit-price/:id/:index',
    loadChildren: () => import('./add-price/add-price.module').then(m => m.AddPricePageModule)
  },
  {
    path: 'add-privacy',
    loadChildren: () => import('./add-privacy/add-privacy.module').then(m => m.AddPrivacyPageModule)
  },
  {
    path: 'edit-privacy/:id',
    loadChildren: () => import('./add-privacy/add-privacy.module').then(m => m.AddPrivacyPageModule)
  },
  {
    path: 'add-footer1',
    loadChildren: () => import('./add-footer1/add-footer1.module').then(m => m.AddFooter1PageModule)
  },
  {
    path: 'add-footer2',
    loadChildren: () => import('./add-footer2/add-footer2.module').then(m => m.AddFooter2PageModule)
  },
  {
    path: 'policy-view/:id',
    loadChildren: () => import('./policy-view/policy-view.module').then(m => m.PolicyViewPageModule)
  },
  {
    path: 'add-title',
    loadChildren: () => import('./add-title/add-title.module').then(m => m.AddTitlePageModule)
  },
  {
    path: 'how-it-works',
    loadChildren: () => import('./how-it-works/how-it-works.module').then(m => m.HowItWorksPageModule)
  },
  {
    path: 'add-how-it-works',
    loadChildren: () => import('./add-how-it-works/add-how-it-works.module').then(m => m.AddHowItWorksPageModule)
  },
  {
    path: 'add-how-it-works/:id',
    loadChildren: () => import('./add-how-it-works/add-how-it-works.module').then(m => m.AddHowItWorksPageModule)
  },
  {
    path: 'edit-how-it-works/:id/:index',
    loadChildren: () => import('./add-how-it-works/add-how-it-works.module').then(m => m.AddHowItWorksPageModule)
  },
  {
    path: 'add-product-cover/:id',
    loadChildren: () => import('./add-product-cover/add-product-cover.module').then(m => m.AddProductCoverPageModule)
  },
  {
    path: 'add-lead',
    loadChildren: () => import('./add-lead/add-lead.module').then(m => m.AddLeadPageModule)
  },
  {
    path: 'edit-lead/:id',
    loadChildren: () => import('./add-lead/add-lead.module').then(m => m.AddLeadPageModule)
  },
  {
    path: 'myleads',
    loadChildren: () => import('./myleads/myleads.module').then(m => m.MyleadsPageModule)
  },
  {
    path: 'mylead-chat/:id',
    loadChildren: () => import('./mylead-chat/mylead-chat.module').then(m => m.MyleadChatPageModule)
  },
  {
    path: 'about-us',
    loadChildren: () => import('./about-us/about-us.module').then(m => m.AboutUsPageModule)
  },
  {
    path: 'view-about-us',
    loadChildren: () => import('./view-about-us/view-about-us.module').then(m => m.ViewAboutUsPageModule)
  },
  {
    path: 'view-leads',
    loadChildren: () => import('./view-leads/view-leads.module').then(m => m.ViewLeadsPageModule)
  },
  {
    path: 'free-user-creation',
    loadChildren: () => import('./free-user-creation/free-user-creation.module').then(m => m.FreeUserCreationPageModule)
  },
  {
    path: 'free-user-creation/:id',
    loadChildren: () => import('./free-user-creation/free-user-creation.module').then(m => m.FreeUserCreationPageModule)
  },
  {
    path: 'manage/:id',
    loadChildren: () => import('./manage/manage.module').then(m => m.ManagePageModule)
  },
  {
    path: 'create-normal-user',
    loadChildren: () => import('./create-normal-user/create-normal-user.module').then(m => m.CreateNormalUserPageModule)
  },
  {
    path: 'edit-normal-user/:id',
    loadChildren: () => import('./create-normal-user/create-normal-user.module').then(m => m.CreateNormalUserPageModule)
  },
  {
    path: 'admin-settings/:id',
    loadChildren: () => import('./admin-settings/admin-settings.module').then(m => m.AdminSettingsPageModule)
  },
  {
    path: 'admin-users',
    loadChildren: () => import('./admin-users/admin-users.module').then(m => m.AdminUsersPageModule)
  },
  {
    path: 'admin-product-and-users/:id',
    loadChildren: () => import('./admin-product-and-users/admin-product-and-users.module').then(m => m.AdminProductAndUsersPageModule)
  },
  {
    path: 'admin-product-details/:adminId/:productId',
    loadChildren: () => import('./admin-product-details/admin-product-details.module').then(m => m.AdminProductDetailsPageModule)
  },  {
    path: 'login-wv',
    loadChildren: () => import('./login-wv/login-wv.module').then( m => m.LoginWvPageModule)
  },

  // {
  //   path: 'viewcomplaints',
  //   loadChildren: () => import('./viewcomplaints/viewcomplaints.module').then( m => m.ViewcomplaintsPageModule)
  // },
  // {
  //   path: 'viewclosedcomplaints',
  //   loadChildren: () => import('./viewclosedcomplaints/viewclosedcomplaints.module').then( m => m.ViewclosedcomplaintsPageModule)
  // },
  // {
  //   path: 'logsyesterday',
  //   loadChildren: () => import('./logsyesterday/logsyesterday.module').then( m => m.LogsyesterdayPageModule)
  // }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
