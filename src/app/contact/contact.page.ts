import { Component, OnInit } from '@angular/core';
import { CrudService } from './../service/crud.service';
import * as firebase from 'firebase';
import { AngularFirestore } from '@angular/fire/firestore';
import { MessageService } from '../service/message.service';
import { CommonService } from '../common.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { PolicyService } from '../service/policy.service';

@Component({
  selector: 'app-contact',
  templateUrl: './contact.page.html',
  styleUrls: ['./contact.page.scss'],
})
export class ContactPage implements OnInit {
  messageObject = {} as any;
  aboutusObject = {} as any;
  public authState: any = null;
  searchArray = []; val1 = ''; val2 = ''; val3 = '';
  constructor(
    private firebaseAuth: AngularFireAuth, private firestore: AngularFirestore,
    public policyService: PolicyService,
    public commonService: CommonService, public messageService: MessageService) { }

  ngOnInit() {
    this.firebaseAuth.authState.subscribe(authState => {
      console.log(authState);
      if (authState) {
        this.authState = authState.uid;
      } else {
        this.authState = null;
      }

    });
    this.policyService.getExtraInfoId('aboutus').subscribe(resp => {
      if (resp) {
        this.aboutusObject = resp;
      }
    })
  }


  saveContact() {
    const washingtonRef = firebase.firestore().collection('contacts').doc('contacts');
    washingtonRef.update({
      regions: firebase.firestore.FieldValue.arrayUnion({ name: this.val1, phno1: this.val2, phno2: this.val3 })
    });
  }

  deleteContact() {
    const washingtonRef = firebase.firestore().collection('contacts').doc('contacts');
    washingtonRef.update({
      regions: firebase.firestore.FieldValue.arrayRemove(
        { name: 'mehak', phno1: '1', phno2: '2' },
      )
    });
  }

  addMessage() {
    this.messageService.addMessage(this.messageObject).then(resp => {
      this.commonService.showAlert('Message sent');
      this.messageService.updateMessage(resp.id, { messageDocId: resp.id, ... this.messageObject }).then(update => {
        this.messageObject = {};
      });
    });
  }

}
