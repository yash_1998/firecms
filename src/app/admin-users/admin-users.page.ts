import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router, ActivatedRoute } from '@angular/router';
import { FeatureService } from '../service/feature.service';
import { CommonService } from '../common.service';
import { Plugins } from '@capacitor/core';
const { Browser } = Plugins;
import * as moment from 'moment';
import { Storage } from '@ionic/storage';

import { ProductService } from '../service/product.service';
import { PolicyService } from '../service/policy.service';
import { CrudService } from '../service/crud.service';


@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.page.html',
  styleUrls: ['./admin-users.page.scss'],
})
export class AdminUsersPage implements OnInit {
  userData = {} as any;
  adminUser = [];
  constructor(
    public crudService: CrudService,
    public router: Router, private storage: Storage, public route: ActivatedRoute, public featureService: FeatureService,
    public commonService: CommonService, public productService: ProductService, public policyService: PolicyService) {
  }

  ngOnInit() {
    this.loadInitData();
    this.router.events.subscribe((event: NavigationStart) => {
      if (event instanceof NavigationStart) {
        if (event.url === '/admin-users') {
          this.loadInitData();
        }
      }
    });

  }

  loadInitData() {
    this.storage.get('loggedInUser').then((val) => {
      if (val) {
        this.crudService.getUserData(val).then(userData => {
          userData.forEach(doc => {
            this.userData = doc.data();
          });
        })
      }
    });
    this.crudService.getAllAdminUser().then(snapshot => {
      this.adminUser = [];
      snapshot.forEach(doc => {
        this.adminUser.push(doc.data());
      });
    });
  }

  createNewUser() {
    this.router.navigate(['free-user-creation', 1]);
  }


  goToProductsAndUsers(item) {
    this.router.navigate(['admin-product-and-users', item.authId]);
  }

}
