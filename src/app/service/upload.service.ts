import { Injectable, Inject } from '@angular/core';
import { AngularFireList, AngularFireDatabase } from '@angular/fire/database';
import * as firebase from 'firebase';


@Injectable({
  providedIn: 'root'
})
export class UploadService {

  imageDetailList: AngularFireList<any>;

  fileList: any[];

  dataSet: Data = {
    id: '',
    url: ''
  };

  msg: string = 'error';

  constructor(@Inject(AngularFireDatabase) private firebaseDb: AngularFireDatabase) { }

  ImageUpload(event, path): Promise<any> {
    return new Promise((resolve, reject) => {
      const storageRef = firebase.storage().ref();
      let uploadTask;

      const file = event;
      uploadTask = storageRef.child(path).put(file);
      this.uploadTask(uploadTask).then(resp => {
        resolve(resp);
      }).catch(error => {
        reject(error);
      });
    });
  }


  uploadTask(uploadTask) {
    return new Promise((resolve, reject) => {
      uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
        (snapshot: any) => {
          // upload in progress
          const percentage = Math.trunc((snapshot.bytesTransferred / snapshot.totalBytes) * 100);
        },
        (error) => {
          // upload failed
          console.log(error);
          reject(error);
        },
        () => {
          // downloading url
          uploadTask.snapshot.ref.getDownloadURL().then(downloadURL => {
            // return downloadedUrl
            resolve(downloadURL);
          }).catch(error => {
            console.log('error : ' + error);
            reject(error);
          });
        }
      );
    });
  }

  getImageDetailList() {
    console.log("get list");
    this.imageDetailList = this.firebaseDb.list('imageDetails');
    console.log(this.imageDetailList);
  }

  insertImageDetails(id, url) {
    this.dataSet = {
      id: id,
      url: url
    };
    this.imageDetailList.push(this.dataSet);
  }

  getImage(value) {
    this.imageDetailList.snapshotChanges().subscribe(
      list => {
        this.fileList = list.map(item => { return item.payload.val(); });
        this.fileList.forEach(element => {
          if (element.id === value)
            this.msg = element.url;
        });
        if (this.msg === 'error')
          alert('No record found');
        else {
          window.open(this.msg);
          this.msg = 'error';
        }
      }
    );
  }
}

export interface Data {
  id: string;
  url: string;
}
