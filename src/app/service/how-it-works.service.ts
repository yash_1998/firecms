import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { uniqueId } from '../constants';


@Injectable({
  providedIn: 'root'
})
export class HowItWorksService {

  constructor(private firestore: AngularFirestore) { }

  addHowItWorks(record) {
    return this.firestore.collection('website').doc(uniqueId).collection('how-it-works').add(record);
  }


  getHowItWorks() {
    return this.firestore.collection('website').doc(uniqueId).collection('how-it-works').valueChanges();
  }


  getHowItWorksById(productId) {
    return this.firestore.collection('website').doc(uniqueId).collection('how-it-works').doc(productId).valueChanges();
  }


  updateHowItWorks(recordID, record) {
    return this.firestore.doc('website/' + uniqueId + '/how-it-works/' + recordID).update(record);
  }



  deleteHowItWorks(record_id) {
    return this.firestore.doc('website/' + uniqueId + '/how-it-works/' + record_id).delete();
  }

}
