import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class FeatureService {
  constructor(
    private firestore: AngularFirestore
  ) { }




  addFeature(productId, record) {
    return this.firestore.collection('products').doc(productId).collection('features').add(record);
  }


  getFeature(productId) {
    return this.firestore.collection('products').doc(productId).collection('features').valueChanges();
  }


  updateFeature(productId, featureId, record) {
    return this.firestore.doc('products/' + productId + '/features/' + featureId).update(record);
  }



  deleteFeature(productId, featureId) {
    return this.firestore.doc('products/' + productId + '/features/' + featureId).delete();
  }

}
