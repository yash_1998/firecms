import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { CommonService } from '../common.service';
import { uniqueId } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class MessageService {

  constructor(
    private firestore: AngularFirestore
  ) { }


  addMessage(data) {
    return this.firestore.collection('website').doc(uniqueId).collection('messages').add(data);
  }

  updateMessage(messageId, data) {
    return this.firestore.collection('website').doc(uniqueId).collection('messages').doc(messageId).update(data);
  }

  getMessages() {
    return this.firestore.collection('website').doc(uniqueId).collection('messages').valueChanges();
  }


  updateFeature(productId, featureId, record) {
    return this.firestore.doc('products/' + productId + '/features/' + featureId).update(record);
  }
}