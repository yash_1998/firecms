import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { uniqueId } from '../constants';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { map, catchError } from 'rxjs/operators';
import { throwError } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class PolicyService {

  constructor(private firestore: AngularFirestore, public http: HttpClient) { }
  addExtraInfo(record) {
    return this.firestore.collection('website').doc(uniqueId).collection('extrainfo').add(record);
  }

  getTempExtreInfo() {
    return this.firestore.collection('extrainfo').valueChanges();
  }

  getExtraInfo() {
    return this.firestore.collection('website').doc(uniqueId).collection('extrainfo').valueChanges();
  }


  getExtraInfoId(productId) {
    return this.firestore.collection('website').doc(uniqueId).collection('extrainfo').doc(productId).valueChanges();
  }


  updateExtraInfo(recordID, record) {
    return this.firestore.doc('website/' + uniqueId + '/extrainfo/' + recordID).update(record);
  }


  createGroup(url, data) {
    return this.http.put(url, data, {
      headers: {
        'Ocp-Apim-Subscription-Key': data.subscription_key,
        'Content-Type': 'application/json',
        'Host': data.host
      }
    }).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }

  errorHandler(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error || 'server error.');
  }

  setExtraInfo(recordID, record) {
    // debugger
    return this.firestore.doc('website/' + uniqueId + '/extrainfo/' + recordID).set(record);
  }

  deleteExtraInfo(recordID) {
    return this.firestore.collection('website').doc(uniqueId).collection('extrainfo').doc(recordID).delete();
  }



  deleteProduct(record_id) {
    return this.firestore.doc('products/' + record_id).delete();
  }
}