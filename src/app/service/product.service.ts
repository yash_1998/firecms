import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore';
import { map, catchError } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { firebaseUrl, uniqueId } from '../constants';
import { throwError, BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {
  public products$: BehaviorSubject<any> = new BehaviorSubject(null);
  constructor(
    private firestore: AngularFirestore,
    public http: HttpClient
  ) { }


  addlead(record) {
    return this.firestore.collection('leads-master').doc(uniqueId).collection('leads').add(record);
  }

  getProfileObs(): Observable<any> {
    return this.products$.asObservable();
  }

  addProuduct(record) {
    return this.firestore.collection('website').doc(uniqueId).collection('products').add(record);
  }

  deleteLead(record_id) {
    return this.firestore.doc('leads-master/' + uniqueId + '/leads/' + record_id).delete();
  }

  getLeads() {
    return this.firestore.collection('leads-master').doc(uniqueId).collection('leads').snapshotChanges();
  }

  setProductUser(productId, userId, data) {
    return this.firestore.collection('website').doc(uniqueId).collection('products').doc(productId).collection('users').doc(userId).set(data);
  }

  updateProductUser(productId, userId, data) {
    return this.firestore.collection('website').doc(uniqueId).collection('products').doc(productId).collection('users').doc(userId).update(data);
  }

  getProductUser(productId, userId) {
    return this.firestore.collection('website').doc(uniqueId).collection('products').doc(productId).collection('users').doc(userId).get().toPromise();
  }

  getProducts() {
    return this.firestore.collection('website').doc(uniqueId).collection('products').valueChanges();
  }


  getProductsById(productId) {
    return this.firestore.collection('website').doc(uniqueId).collection('products').doc(productId).valueChanges();
  }

  getProductsByIdGet(productId) {
    return this.firestore.collection('website').doc(uniqueId).collection('products').doc(productId).get();
  }



  getLeadById(leadId) {
    return this.firestore.collection('leads-master').doc(uniqueId).collection('leads').doc(leadId).valueChanges();
  }

  updateProduct(recordID, record) {
    return this.firestore.doc('website/' + uniqueId + '/products/' + recordID).update(record);
  }

  setProduct(recordID, record) {
    return this.firestore.doc('website/' + uniqueId + '/products/' + recordID).set(record);
  }
  updateLeads(recordID, record) {
    return this.firestore.doc('leads-master/' + uniqueId + '/leads/' + recordID).update(record);
  }

  setLeads(recordID, record) {
    return this.firestore.doc('leads-master/' + uniqueId + '/leads/' + recordID).set(record);
  }



  deleteProduct(record_id) {
    return this.firestore.doc('website/' + uniqueId + '/products/' + record_id).delete();
  }



  cancelRazorPaySubscriptions(data) {
    return this.http.post(firebaseUrl + 'cancelRazorpay', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }

  getRazorpaySubscribtionsById(data) {
    return this.http.post(firebaseUrl + 'getRazorpaySubscriptionsById', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }

  cancelStripeSubscriptions(data) {
    return this.http.post(firebaseUrl + 'cancelStripeSubscriptions', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }



  createStripeSubscribtions(data) {
    return this.http.post(firebaseUrl + 'createStripeSubscriptions', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }


  createStripeProduct(data) {
    return this.http.post(firebaseUrl + 'createStripeProduct', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }

  updateStripeProduct(data) {
    return this.http.post(firebaseUrl + 'updateStripeProduct', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }


  createPlan(data) {
    return this.http.post(firebaseUrl + 'createRazorpayPlans', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }

  createSubscribtions(data) {
    return this.http.post(firebaseUrl + 'createRazorpaySubscriptionLink', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }


  createStripeCustomers(data) {
    return this.http.post(firebaseUrl + 'createStripeCustomers', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }
  createStripePrice(data) {
    return this.http.post(firebaseUrl + 'createStripeProductPrice', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }

  createStripeOneTimePrice(data) {
    return this.http.post(firebaseUrl + 'createStripeOneTimePrice', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));

  }
  updateStripePrice(data) {
    return this.http.post(firebaseUrl + 'updateStripeProductPrice', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }

  createStripeSessions(data) {
    return this.http.post(firebaseUrl + 'createStripeSessions', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));

  }

  createStripeOneTimeSessions(data) {
    return this.http.post(firebaseUrl + 'stripeOneTimePayment', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));

  }

  getSessionDataById(data) {
    return this.http.post(firebaseUrl + 'createStripeSessionsDataById', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }

  getStripeSubscribtionsById(data) {
    return this.http.post(firebaseUrl + 'getStripeSubscriptionsById', data).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }

  getLocationDetails() {
    return this.http.post(firebaseUrl + 'getLocationFromIp', {}).pipe(
      map((response: any) => response), catchError(this.errorHandler));

  }


  errorHandler(error: HttpErrorResponse) {
    console.log(error);
    return throwError(error || 'server error.');
  }

  train(url, data, settings) {
    return this.http.post(url, data, {
      "headers": {
        "ocp-apim-subscription-key": settings.subscription_key,
        "content-type": "application/json",
        "host": settings.host,
        "cache-control": "no-cache",
        "postman-token": "f6adc04e-aa7a-61f8-c072-ce43de7dc1c5"
      }
    }).pipe(
      map((response: any) => response), catchError(this.errorHandler));
  }


}
