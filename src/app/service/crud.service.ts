import { Injectable } from '@angular/core';

import { AngularFirestore } from '@angular/fire/firestore';
import { ToastController, LoadingController } from '@ionic/angular';
import { uniqueId } from '../constants';

@Injectable({
  providedIn: 'root'
})
export class CrudService {


  constructor(
    public loadingController: LoadingController,
    private firestore: AngularFirestore,
    public toastController: ToastController
  ) { }

  create_NewStudent(record) {
    return this.firestore.collection('master').doc(uniqueId).collection('Students').add(record);
  }

  getUsrByCompanyName(name) {
    return this.firestore.collection('master').doc(uniqueId).collection('Students').ref.where('company_code', '==', name).get();
  }


  removeStudent(recordID) {
    return this.firestore.doc('master/' + uniqueId + '/Students/' + recordID).delete();
  }

  getTempProducts() {
    return this.firestore.collection('products').valueChanges();
  }

  getTempLeads() {
    return this.firestore.collection('leads').valueChanges();
  }

  setNewStudents(recordID, record) {
    return this.firestore.doc('master/' + uniqueId + '/Students/' + recordID).set(record);
  }

  setNewStudentsSettings(recordID, product, record) {
    return this.firestore.doc('master/' + uniqueId + '/Students/' + recordID + '/settings/' + product).set(record);
  }

  getNewStudentsSettings(recordID, product) {
    return this.firestore.doc('master/' + uniqueId + '/Students/' + recordID + '/settings/' + product).valueChanges();
  }

  updateNewStudents(recordID, record) {
    return this.firestore.doc('master/' + uniqueId + '/Students/' + recordID).update(record);
  }

  addReminder(record) {
    return this.firestore.collection('leads-master').doc(uniqueId).collection('reminders').add(record);
  }


  getAllAdminUser() {
    return this.firestore.collection('master').doc(uniqueId).collection('Students').ref.where('role', '==', 'admin').get();
  }

  addActiveProduct(record) {
    return this.firestore.collection('master').doc(uniqueId).collection('activeproducts').add(record);
  }

  updateActiveProducts(id, record) {
    return this.firestore.doc('master/' + uniqueId + '/activeproducts/' + id).update(record);
  }

  getUserLeads(userId) {
    return this.firestore.collection('leads-master').doc(uniqueId).collection('leads').ref.where('userId', '==', userId).get();
  }


  getActiveProduct(userId?) {
    return this.firestore.collection('master').doc(uniqueId).collection('activeproducts').snapshotChanges();
  }

  getActiveProductById(productId) {
    return this.firestore.collection('master').doc(uniqueId).collection('activeproducts').ref.where('productDocId', '==', productId).get();
  }


  deleteActiveProduct(docId) {
    return this.firestore.collection('master').doc(uniqueId).collection('activeproducts').doc(docId).delete();
  }

  getUsers() {
    return this.firestore.collection('master').doc(uniqueId).collection('Students').snapshotChanges();
  }

  getUsersById(id) {
    return this.firestore.collection('master').doc(uniqueId).collection('Students').doc(id).valueChanges();
  }

  getNormalUsersByAdminId(id) {
    return this.firestore.collection('master').doc(uniqueId).collection('Students').ref.where('adminUserId', '==', id).get();
  }

  getActiveProductByAdminId(id) {
    return this.firestore.collection('master').doc(uniqueId).collection('activeproducts').ref.where('userId', '==', id).get();
  }

  getActiveProductData(productId) {
    return this.firestore.collection('master').doc(uniqueId).collection('activeproducts').ref.where('productDocId', '==', productId).get();
  }

  getUserData(userId) {
    return this.firestore.collection('master').doc(uniqueId).collection('Students').ref.where('authId', '==', userId).get();
  }

  async showToast(message) {
    const toast = await this.toastController.create({
      message,
      duration: 5000
    }).then((toastData) => {
      toastData.present();
    });
  }


  create_NewCover(record) {
    return this.firestore.collection('website').doc(uniqueId).collection('homemgmt').add(record);
  }


  getNewCover() {
    return this.firestore.collection('website').doc(uniqueId).collection('homemgmt').valueChanges();
  }


  updateNewCover(recordID, record) {
    return this.firestore.doc('website/' + uniqueId + '/homemgmt/' + recordID).update(record);
  }



  deleteNewCover(record_id) {
    return this.firestore.doc('website/' + uniqueId + '/homemgmt/' + record_id).delete();
  }


  showLoader() {

    this.loadingController.create({
      message: ''
    }).then((res) => {
      res.present();
    });

  }

  hideLoader() {

    this.loadingController.dismiss().then((res) => {
      console.log('Loading dismissed!', res);
    }).catch((error) => {
      console.log('error', error);
    });

  }


  manageActiveProduct(productId, userId) {

    return new Promise((resolve, reject) => {
      this.getActiveProductById(productId).then(snapshot => {
        const tempData = [];
        snapshot.forEach(doc => {
          const docData = { ...doc.data(), activeProductId: doc.id } as any;
          if (docData.userId === userId) {
            tempData.push(docData);
          }
        })
        const result = tempData.map(async (item, index) => {
          try {
            return await this.deleteActiveProduct(item.activeProductId)
          } catch (e) {
            return true;
          }
        });
        Promise.all(result).then(promise => {
          resolve(true);
        }).catch(e => {
          resolve(true);
        })
      }).catch(e => {
        resolve(true);
      })
    })
  }


}
