import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';
import { NavController } from '@ionic/angular';
import { MenuController } from '@ionic/angular';
import { AuthenticateService } from '../service/authentication.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { CrudService } from '../service/crud.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  validations_form: FormGroup;
  errorMessage: string = '';

  constructor(
    public crudService: CrudService,
    private navCtrl: NavController,
    private authService: AuthenticateService,
    private formBuilder: FormBuilder,
    private menuCtrl: MenuController,
    private storage: Storage,
    public router: Router

  ) {

  }

  ngOnInit() {
    this.storage.get('loggedInUser').then((val) => {
      console.log('L-Your user ID is', val);
      if (val != null || "") {
        console.log("L-some one is logged in");
        this.navCtrl.navigateForward('/dashboard');
        //  this.navCtrl.navigateRoot('/dashboard');
      } else {
        console.log("L-no one is logged in");
      }
    });



    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
  }


  validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Please enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 6 characters long.' }
    ]
  };


  loginUser(value) {

    // this.storage.get('loggedInUser').then((val) => {
    //   console.log('Your user ID is', val);
    // });

    this.authService.loginUser(value)
      .then(res => {
        console.log(res, 'res');
        this.errorMessage = "";
        if (res && res.user && res.user.emailVerified) {
          this.storage.set('userEmail', res.user.email);
          this.storage.set('loggedInUser', res.user.uid).then(resp => {
            this.navCtrl.navigateForward('/home');
          });

        } else {
          this.crudService.showToast('Email is not verified')
        }
        //this.storage.set('loggedInUser', this.userId); do it
        //this.storage.set('userEmail', this.userEmail); do it
        // this.navCtrl.navigateForward('/dashboard');
      }, err => {
        this.errorMessage = err.message;
      })
  }


  goToRegisterPage() {
    this.navCtrl.navigateForward('/register');
  }

  goToHomePage() {
    this.router.navigate(['home']);
  }

}
