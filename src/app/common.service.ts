import { Injectable } from '@angular/core';
import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { AlertController, ToastController, ModalController, LoadingController } from '@ionic/angular';
import { Plugins } from '@capacitor/core';

const { Storage } = Plugins;

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  primaryColor = '';
  loading: any;
  constructor(
    public alert: AlertController,
    public toastController: ToastController,
    public modalController: ModalController,
    public loadingController: LoadingController,
    public diagnose: Diagnostic
  ) { }

  checkGPS() {
    return new Promise(async (resolve: any) => {
      this.diagnose.isLocationEnabled().then((data: any) => {
        resolve(data);
      });
    });
  }

  async showToast(count) {
    const toast = await this.toastController.create({
      message: 'Getting GPS location: Retry ' + count,
      duration: 2000
    });
    toast.present();
  }

  async presentIndicator() {
    const message = '';
    this.loading = await this.loadingController.create({
      message,
      duration: 4000,
      cssClass: 'custom-loading'
    });
    await this.loading.present();
  }

  async dismissIndicator() {
    return new Promise(async (resolve, reject) => {
      if (this.loading) {
        await this.loading.dismiss().then(resp => {
          resolve(resp);
        });
      }
    });
  }


  async presentModal(ModalPage, data, cssClass) {
    const modal = await this.modalController.create({
      component: ModalPage,
      componentProps: { data },
      cssClass
    });
    return await modal.present();
  }

  async setObject(key, data) {
    await Storage.set({
      key,
      value: JSON.stringify(data)
    });
  }

  // JSON "get" example
  async getObject(key) {
    const ret = await Storage.get({ key });
    const data = JSON.parse(ret.value);
    return data;
  }

  async showAlert(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }



  async showActionAlert(msg?: any) {
    return new Promise(async (resolve: any) => {
      const alert = await this.alert.create({
        header: 'Error!',
        message: msg,
        animated: true,
        mode: 'ios',
        backdropDismiss: false,
        buttons: [
          {
            text: 'Try Again',
            handler: () => {
              resolve(true);
            }
          },
          {
            text: 'Cancel',
            handler: () => {
              resolve(false);
            }
          }
        ]
      });

      await alert.present();
    });

  }
}
