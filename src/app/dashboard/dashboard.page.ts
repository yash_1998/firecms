import { Component, OnInit } from '@angular/core';


import { NavController, ModalController, MenuController } from '@ionic/angular';
import { AuthenticateService } from '../service/authentication.service';
import * as moment from 'moment';
import { IonicStorageModule } from '@ionic/storage';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase';
import { UserService } from '../user.service';
import { AngularFirestore } from '@angular/fire/firestore';


// for popover
import { PopoverController } from '@ionic/angular';
// import {NotificationsPage} from '../popups/notifications/notifications.page';

// notification toast
import { ToastController } from '@ionic/angular';
import { reduce, filter } from 'rxjs/operators';
import { CrudService } from '../service/crud.service';
import { SelectPriceComponent } from '../select-price/select-price.component';
import { ProductService } from '../service/product.service';
import { Router, RouterEvent, NavigationStart } from '@angular/router';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.page.html',
  styleUrls: ['./dashboard.page.scss'],

})
export class DashboardPage implements OnInit {

  loggedInuser = ''; notifyarr: any; notification_length = 0; i: any;
  userEmail: string;
  userId: string;
  userData = {} as any;
  userNm = ''; userlNm = ''; userAllowedPages = [];
  followers = ['brown', 'black'];
  myProducts = [];
  products: any;
  constructor(
    public toastController: ToastController,
    private popover: PopoverController,
    public prouductService: ProductService,
    private navCtrl: NavController,
    private authService: AuthenticateService,
    public crudService: CrudService,
    private storage: Storage,
    public modalController: ModalController,
    private firestore: AngularFirestore,
    public router: Router,
    private menuCtrl: MenuController,
    public user: UserService
  ) {
  }

  goToCompanySettings() {
    this.navCtrl.navigateForward('/company-setting');
  }

  ngAfterViewInit() {
    // this.cdRef.detectChanges();
  }

  //  createpopup(i){
  //   console.log(i);
  //       this.popover.create({
  //         component:NotificationsPage,
  //         componentProps: { notification : this.notifyarr},
  //         cssClass: 'custom-popover',
  //         showBackdrop:false}).then((popoverElement)=>{
  // //pass something here
  //       popoverElement.present();
  //       })
  //     }

  async ngOnInit() {
    this.loadInitData();
    this.router.events.subscribe((value) => {
      if (value instanceof NavigationStart) {
        if (value.url === '/dashboard') {
          this.loadInitData();
        }
      }
    })
  }

  loadInitData() {
    this.prouductService.products$.subscribe(resp => {
      this.getProductData();
    })
    this.storage.set('allowedPages', [1, 2, 3, 4]);
    this.storage.get('allowedPages').then((val) => {
      console.log('Your pages is', val);
    });


    this.menuCtrl.enable(true);
    // Or to get a key/value pair
    this.storage.get('userEmail').then((val) => {
      console.log('Your mail is', val);
      this.userEmail = val;
    });
    this.storage.get('userName').then((val) => {
      console.log('Your F Name is', val);
    });

    this.storage.get('userlName').then((val) => {
      console.log('Your L name is', val);
    });

    // this.storage.get('loggedInUser').then((val) => {
    //   console.log('Your user ID is', val);
    //   this.loggedInuser = val;
    // });

    this.storage.get('loggedInUser').then((val) => {
      if (val) {
        this.crudService.getActiveProduct(val).subscribe(resp => {
          this.myProducts = [];
          resp.forEach((actions) => {
            const body: any = actions.payload.doc.data();
            if (body.userId === val) {
              this.myProducts.push(body);
            }
          })
          console.log(this.myProducts);
        });
        this.crudService.getUserData(val).then(userData => {
          console.log(userData);
          userData.forEach(doc => {
            this.userData = doc.data();
            console.log(doc.data());
          })
          if (!this.userData.stripe_customer_id) {
            this.createStripeCustomer()
          }
          this.getProductData();
        })
      } else {
        this.getProductData();
      }
      if (val != null) {
        console.log('some one is logged in');
        this.loggedInuser = val;
        console.log(this.loggedInuser);
        this.doc_check();
        // this.fetch_notification();

        //   this.navCtrl.navigateForward('/dashboard');
        // this.navCtrl.navigateRoot('/dashboard');
      } else {
        console.log('no one is logged in');
        //    this.storage.set('loggedInUser', this.userId);
        //    this.storage.set('userEmail', this.userEmail);
        // this.navCtrl.navigateBack('');
      }
      console.log('Your user ID is', val);
    });
    console.log(this.loggedInuser, this.authService.userDetails());
    if (this.authService.userDetails()) {
      this.userEmail = this.authService.userDetails().email;
      this.userId = this.authService.userDetails().uid;
      console.log(this.userId);
      this.storage.set('loggedInUser', this.userId);
      this.storage.set('userEmail', this.userEmail);
      this.loggedInuser = this.userId;

      this.doc_check();

      //   this.storage.set('userName', this.user.registered_username);

      console.log('trying...');
      firebase.firestore().collection(`Students`)
        .where('authId', '==', this.userId)
        .get()
        .then(querySnapshot => {
          querySnapshot.forEach((doc) => {
            console.log(doc.data().Name);
            this.userNm = doc.data().Name;
            this.userlNm = doc.data().lname;
            this.userEmail = doc.data().personalEmail;
            this.userAllowedPages = doc.data().allowedpages;
            this.storage.set('userName', this.userNm);
            this.storage.set('userlName', this.userlNm);
            this.storage.set('userEmail', this.userEmail);
            console.log(this.userNm, this.userlNm);
            console.log(this.userAllowedPages);
            this.storage.set('allowedPages', this.userAllowedPages);
            console.log(this.userAllowedPages);
            console.log(doc.id, ' ===> ', doc.data());
          });
        });
      setTimeout(() => {
        console.log(this.userNm);
      }, 1000);
      // this.storage.set('usernm',this.userNm);

      // var worksRef = firebase.firestore().collection("todos")
      // .where("authId", "==", this.userId)
      // .get().subscribe(querySnapshot => {
      //  console.log(query.data());

      // firebase.firestore().collection("Students").where("authId", "==", this.userId)
      //     .onSnapshot(function(querySnapshot) {
      //         querySnapshot.forEach(function(doc) {
      //           console.log("--name--");
      //             console.log(doc.data().name);
      //         });

      //     });


    }

    else {
      // var allowedPages = this.storage.get('allowedPages');
      // console.log(allowedPages);
      this.storage.get('loggedInUser').then((val) => {
        if (val == null || '') {
          console.log('First time naughty, go back to login');
          this.navCtrl.navigateBack('');
        }
      });

    }
  }

  getProductData() {
    this.products = [];
    this.prouductService.getProducts().subscribe(resp => {
      this.products = [];
      resp.map((data: any) => {
        if (data.status === 'active') {
          this.products.push(data);
        }

      });
      // console.log(this.products, 'this.products')
      let count = 0;
      for (let i = 0; i < this.products.length; i++) {
        const productData = this.products[i];
        if (this.userData.authId) {
          this.prouductService.getProductUser(productData.productDocId, this.userData.authId).then(userData => {
            const userProductData = userData.data();
            // console.log(userProductData, 'userProductData', count)
            if (userProductData) {
              this.products[count].paid_user = userProductData.paid_user;
              this.products[count].end_date = userProductData.end_date;
              this.products[count].start_date = userProductData.start_date;
              this.products[count] = { ...this.products[count], ...userProductData }
            }
            count++;
            if (count === (this.products.length - 1)) {
              // this.products.sort((a, b) => parseInt(a.orderby) - parseInt(b.orderby));
            }
          })
        }
      }
      //   
      console.log(this.products, 'dashboard product');
    });
  }


  goToMyLeads() {
    this.navCtrl.navigateForward('/myleads');
  }

  goToAddLead() {
    this.navCtrl.navigateForward('/add-lead');
  }

  goToHomeManagement() {
    this.navCtrl.navigateForward('/home-management');
  }

  goToAdminUsers() {
    this.navCtrl.navigateForward('/admin-users');
  }

  goToView() {
    this.navCtrl.navigateForward('/view-leads');
  }

  goToProduct() {
    this.navCtrl.navigateForward('/add-product');
  }


  goToManage(item) {
    this.router.navigate(['manage', item.productDocId]);
  }

  goToFeature() {
    this.navCtrl.navigateForward('/product-management');
  }

  addTitle() {
    this.navCtrl.navigateForward('/add-title');
  }


  async presentModal(item) {
    const modal = await this.modalController.create({
      component: SelectPriceComponent,
      cssClass: 'my-custom-class price-modal',
      componentProps: { userData: this.userData, productInfo: item }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      this.getProductData();
    }
  }

  getDateFormat(data) {
    if (data.end_date) {
      return moment(data.end_date).format('DD/MM/YYYY');
    }
  }

  getNumberOfDays(data) {
    const endDate = moment(data.end_date).valueOf();
    const todayDate = moment().valueOf();
    if (endDate >= todayDate) {
      return (this.diffDays(endDate, todayDate)) + 'days remaining';
    } else {
      return 'Product subscription expired';
    }
  }


  getNumberOfDaysValue(data) {
    const endDate = moment(data.end_date).valueOf();
    const todayDate = moment().valueOf();
    if (endDate >= todayDate) {
      return this.diffDays(endDate, todayDate);
    } else {
      return -1;
    }
  }

  diffDays(d1, d2) {
    var ndays;

    ndays = (d2 - d1) / 1000 / 86400;
    ndays = Math.round(ndays - 0.5);
    return Math.abs(ndays);
  };

  logout() {
    console.log('clicked');
    this.authService.logoutUser()
      .then(res => {
        console.log(res);
        this.storage.set('loggedInUser', null);
        this.storage.set('userEmail', null);
        this.storage.set('userName', null);
        this.storage.set('userlName', null);
        this.storage.set('allowedPages', null);

        this.navCtrl.navigateBack('');
      })
      .catch(error => {
        console.log(error);
      });
  }

  // 1. check if the doc exists
  doc_check() {

    const docRef = firebase.firestore().collection('notifications').doc(this.loggedInuser);
    docRef.get().then(doc => {
      if (doc.exists) {
        console.log('Document data:', doc.data());
      } else {
        // doc.data() will be undefined in this case
        console.log('No such document!');
        this.create_doc();
      }
    }).catch((error) => {
      console.log('Error getting document:', error);
    });
  }

  // 2.create doc
  create_doc() {
    firebase.firestore().collection('notifications').doc(this.loggedInuser).set({
      //  name: "Los Angeles visit",
      notifyArray: [],
      // age: 12,
    })
      .then(() => {
        console.log('Document successfully written!');
      })
      .catch((error) => {
        console.error('Error writing document: ', error);
      });

  }

  goToProductInfo(i) {

  }

  freeProudct(product) {

    const productBody = {} as any;
    const start = moment().format();
    const end = moment().add(14, 'd').format();
    productBody.userId = this.userData.authId;
    productBody.key = product.key ? product.key : '';
    productBody.start_date = start;
    productBody.end_date = end;
    productBody.paid_user = false;
    const activeProductBody = {} as any;
    activeProductBody.start_date = start;
    activeProductBody.end_date = end;
    activeProductBody.productDocId = product.productDocId;
    activeProductBody.product_name = product.product_name;
    activeProductBody.product_image = product.product_image;
    activeProductBody.key = product.key ? product.key : '';
    if (product.prices) {
      activeProductBody.prices = product.prices;
    }
    if (product.features) {
      activeProductBody.features = product.features;
    }

    activeProductBody.userId = this.userData.authId;
    activeProductBody.product_description = product.product_description;
    activeProductBody.paid_user = false;
    this.prouductService.setProductUser(product.productDocId, this.userData.authId, productBody).then(resp => {
      this.crudService.showToast(`Free trial started for ${product.product_name}`);
      this.getProductData();
    })
    this.crudService.manageActiveProduct(product.productDocId, this.userData.authId).then(resp => {
      if (resp) {
        this.crudService.addActiveProduct(activeProductBody).then(resp => {
          this.crudService.updateActiveProducts(resp.id, { activeProductDocId: resp.id, ...activeProductBody }).then(resp => {

          });
        });
      }
    })

    console.log(product, productBody, activeProductBody);
  }


  bell() {

    console.log('call bell');
    this.bellagain();
  }
  bellagain() {
    if (this.user.stopbell === 'Y') {
      const audio = new Audio();
      audio.src = 'assets/audio/clearly.mp3';
      audio.load();
      audio.play();

      // sample testing for order..
      // setTimeout(()=>{
      //   if(this.user.stopbell == 'Y')
      //   {
      //     audio.play();
      //     this.bellagain();
      //   }

      // },5000);
    }
  }

  stopbell() {
    if (this.user.stopbell === 'Y') {
      this.user.stopbell = 'N';
    }
    else {
      this.user.stopbell = 'Y';
    }

  }

  createStripeCustomer() {
    this.prouductService.createStripeCustomers({ Name: this.userData.Name, email: this.userData.email, phone: this.userData.phno }).subscribe(resp => {
      if (resp) {
        const parseData = JSON.parse(resp.data);
        console.log(parseData);
        this.crudService.updateNewStudents(this.loggedInuser, { stripe_customer_id: parseData.id, ...this.userData }).then(resp => {

        })
      }
    });
  }




  async notificationToast() {
    const toast = await this.toastController.create({
      header: 'Toast header',
      message: 'New Notification: ' + this.notifyarr[this.notification_length].desc,
      cssClass: 'toast-scheme ',
      color: 'warning',
      buttons: [{
        //    side: 'start',
        //    icon: 'star',
        text: 'Hide',
        handler: () => {
          console.log('Favorite clicked');
        }
      }],
      position: 'top',
      duration: 2000
    });
    toast.present();
  }


}
