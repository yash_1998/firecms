import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router, ActivatedRoute } from '@angular/router';
import { FeatureService } from '../service/feature.service';
import { CommonService } from '../common.service';
import { Plugins } from '@capacitor/core';
const { Browser } = Plugins;
import * as moment from 'moment';
import { Storage } from '@ionic/storage';

import { ProductService } from '../service/product.service';
import { PolicyService } from '../service/policy.service';
import { CrudService } from '../service/crud.service';

declare var Razorpay;
@Component({
  selector: 'app-product-detail',
  templateUrl: './product-detail.page.html',
  styleUrls: ['./product-detail.page.scss'],
})
export class ProductDetailPage implements OnInit {
  productInfo = {} as any;
  selectedCover = true;
  footerObject = {} as any;
  userData = {} as any;
  productId = '';
  products = [];
  feature = [];
  paidUser = false;
  purchaseData = {} as any;
  body: any;
  selectedPrice = 'monthly';
  myProducts: any[] = [];
  ipData = {} as any;
  filterPrice = [];
  siteType = '';
  constructor(
    public crudService: CrudService,
    public router: Router, private storage: Storage, public route: ActivatedRoute, public featureService: FeatureService,
    public commonService: CommonService, public productService: ProductService, public policyService: PolicyService) {
  }

  ngOnInit() {
    if (window.location.origin) {
      const findIndex = window.location.origin.search('localhost');
      if (findIndex >= 0) {
        this.siteType = 'localhost';
      } else {
        this.siteType = 'original';
      }
    }
    console.log(window.location.origin);
    this.storage.get('loggedInUser').then((val) => {
      if (val) {
        this.crudService.getActiveProduct().subscribe(resp => {
          this.myProducts = [];
          resp.forEach((actions) => {
            const body: any = actions.payload.doc.data();
            if (body.userId === val) {
              this.myProducts.push(body);
            }
          })
          console.log(this.myProducts);
        });
        this.crudService.getUserData(val).then(userData => {
          console.log(userData);
          userData.forEach(doc => {
            this.userData = doc.data();
            console.log(doc.data());
          })
          this.getProductData();
        })
      } else {
        this.getProductData();
      }
    });
    this.policyService.getExtraInfo().subscribe(doc => {
      doc.map((data: any) => {
        this.footerObject = {
          ...this.footerObject,
          [data.id]: data
        };
      });
    });
    this.route.queryParams.subscribe(params => {
      this.productId = this.route.snapshot.params.id;
      if (params.session_id) {
        this.commonService.presentIndicator().then(loader => {
          this.productService.getSessionDataById({ session_id: params.session_id }).subscribe(sessionData => {
            if (sessionData.status) {
              if (params.prev_subscription_id) {
                this.productService.cancelStripeSubscriptions({ prev_subscription_id: params.prev_subscription_id }).subscribe(resp => {

                });
              }
              if (sessionData.data && sessionData.data.subscription) {
                this.productService.getStripeSubscribtionsById({ subscription_id: sessionData.data.subscription }).subscribe(subData => {
                  if (subData) {
                    this.getProductData('success', { ...params, subscription_id: sessionData.data.subscription, end_date: subData.data.current_period_end * 1000 });
                  } else {
                    this.getProductData('success', { ...params, subscription_id: sessionData.data.subscription });
                  }

                })
              } else {
                this.getProductData('success', { ...params, end_date: moment().add(100, 'y').format() });
              }


            } else {
              this.crudService.showToast('something is wrong. please contact support');
            }
          })
        })
      }
      if (this.router.getCurrentNavigation().extras.state) {
        this.productInfo = this.router.getCurrentNavigation().extras.state.productInfo;
      }
    });
  }


  segmentChanged(event) {
    this.selectedPrice = event.target.value;
    this.getProductData();
  }

  getProductData(success?, params?) {
    this.products = [];
    this.productService.getProductsByIdGet(this.productId).subscribe(resp => {
      this.productInfo = resp.data();
      this.filterPrice = [...this.productInfo.prices];
      this.productService.getLocationDetails().subscribe(resp => {
        let filterData = [];
        this.ipData = resp.data;
        if (this.ipData.country !== 'IN') {
          filterData = this.filterPrice.filter(item => {
            return item.currency === 'USD' && this.selectedPrice === item.plan.period
          });
        } else {
          filterData = this.filterPrice.filter(item => {
            return item.currency === 'INR' && this.selectedPrice === item.plan.period
          });
        }
        this.filterPrice = [...filterData];

      })
      if (success) {
        setTimeout(() => {
          this.changeUserStatus({ session_id: params.session_id, subscription_id: params.subscription_id, end_date: params.end_date }, this.filterPrice[parseInt(params.index)]);
        }, 2000);

      }
      if (this.userData.authId && this.productInfo.productDocId) {
        this.productService.getProductUser(this.productInfo.productDocId, this.userData.authId).then(resp => {
          const data = resp.data();
          if (data) {
            this.paidUser = data.paid_user;
            this.purchaseData = data;
          }
        })
        // const findIndex = this.productInfo.users.findIndex(item => item.userId === this.userData.authId);
        // if (findIndex > -1) {
        //   this.paidUser = this.productInfo.users[findIndex].paid_user;
        //   this.purchaseData = this.productInfo.users[findIndex];
        //   console.log(this.purchaseData, 'purchase data');
        // }

      }
      console.log(this.productInfo);
    });
  }


  getDateFormat() {
    if (this.purchaseData.end_date) {
      return moment(this.purchaseData.end_date).format('DD/MM/YYYY');
    }
  }

  buyNow(item, i) {
    this.commonService.presentIndicator();
    if (item.stripe_price_id) {
      if (item.plan && item.plan.period !== 'one_time') {
        this.productService.createStripeSessions({ prev_subscription_id: this.purchaseData.subscription_id, siteType: this.siteType, index: i, product_id: this.productInfo.productDocId, stripe_customer_id: this.userData.stripe_customer_id, stripe_price_id: item.stripe_price_id }).subscribe(resp => {
          this.commonService.dismissIndicator();
          const parseData = JSON.parse(resp.data);
          if (parseData.url) {
            window.open(parseData.url, '_self');
          }
        }, (e) => {
          const parseData = JSON.parse(e.data);
        });
      } else {
        this.productService.createStripeOneTimeSessions({ siteType: this.siteType, index: i, product_id: this.productInfo.productDocId, stripe_customer_id: this.userData.stripe_customer_id, stripe_price_id: item.stripe_price_id }).subscribe(resp => {
          this.commonService.dismissIndicator();
          const parseData = resp.data;
          if (parseData.url) {
            window.open(parseData.url, '_self');
          }
        }, (e) => {
          const parseData = JSON.parse(e.data);
          console.log(parseData);
        });
      }


    } else {
      if (item.plan && item.plan.period !== 'one_time') {
        this.productService.createSubscribtions(item).subscribe(resp => {
          this.commonService.dismissIndicator();
          try {
            const parseData = JSON.parse(resp.data);
            this.razorpayMethod(parseData, item);
          } catch (e) {

          }
          console.log(resp);
        });
      } else {
        this.razorpayMethod({}, item);
      }
    }
  }



  razorpayMethod(parseData, item) {
    const options = {
      key: 'rzp_live_qAGcCTrXcDTcJQ', // Enter the Key ID generated from the Dashboard
      name: 'Officeplus',
      subscription_id: parseData.id ? parseData.id : null,
      currency: 'INR',
      description: 'Test Transaction',
      external: {
        wallets: ['paytm', 'amazonpay', 'citrus'],
        handler: (data) => {
          console.log(data);
        }
      },
      image: 'https://firebasestorage.googleapis.com/v0/b/digilyceum-15c27.appspot.com/o/UploadedImages%2Frazorpay.svg?alt=media&token=2c5957bb-dcfe-4759-aa53-6d145b361116',
      handler: (response) => {
        console.log(response);
        if (response.razorpay_payment_id) {
          this.commonService.presentIndicator();
          if (this.purchaseData.subscription_id) {
            this.productService.cancelRazorPaySubscriptions({ prev_subscription_id: this.purchaseData.subscription_id }).subscribe(data => {
              console.log(data);
            });
          }
          if (parseData.id) {
            this.productService.getRazorpaySubscribtionsById({ subscription_id: parseData.id }).subscribe(resp => {
              item.end_date = moment(resp.data.current_end * 1000).add(14, 'd').format();
              this.changeUserStatus(response, { subscription_id: parseData.id, ...item });
            });
          } else {
            item.end_date = moment().add(100, 'y').format();
            this.commonService.dismissIndicator();
            this.changeUserStatus(response, { ...item });
          }

        }
        // alert(response.razorpay_payment_id);
        // alert(response.razorpay_order_id);
        // alert(response.razorpay_signature);
      },
      prefill: {
        // name: 'Gaurav Kumar',
        // email: 'gaurav.kumar@example.com',
        // contact: '9999999999'
      },
      notes: {
        address: 'Razorpay Corporate Office'
      },
      theme: {
        color: '#F37254'
      }
    } as any;
    if (!parseData.id) {
      delete options.subscription_id;
      options.amount = item.price * 100;
    }
    console.log(options)
    const rzp1 = new Razorpay(options);
    rzp1.open();

  }


  changeUserStatus(response, item) {
    if (response.subscription_id || item.subscription_id) {
      this.crudService.showToast(`your transaction is successfully your subscription started for ${this.productInfo.product_name}`);
    } else {
      this.crudService.showToast(`your transaction is successfully for ${this.productInfo.product_name}`);
    }

    const findIndex = this.myProducts.findIndex((item) => item.productDocId === this.productInfo.productDocId);
    let body = {} as any;
    if (findIndex > -1) {
      body = { ...this.myProducts[findIndex] }
    }
    body.paid_user = true;
    if (response.razorpay_payment_id) {
      body.razorpay_payment_id = response.razorpay_payment_id;
    }
    if (response.session_id) {
      body.session_id = response.session_id;
    }
    if (item.end_date) {
      body.end_date = item.end_date;
    }
    if (response.end_date) {
      body.end_date = moment(response.end_date).add('14', 'd').format();
    }
    if (response.subscription_id) {
      body.subscription_id = response.subscription_id;
    }
    if (this.productInfo.key) {
      body.key = this.productInfo.key;
    }
    if (item.subscription_id) {
      body.subscription_id = item.subscription_id;
    }
    body.time = moment().format();
    body.plan = item.plan;
    body.price = item.price;
    if (item.no_user) {
      body.no_user = item.no_user;
    }

    body.productDocId = this.productInfo.productDocId;
    body.product_name = this.productInfo.product_name;
    body.product_image = this.productInfo.product_image;
    body.prices = this.productInfo.prices;
    body.features = this.productInfo.features ? this.productInfo.features : '';
    body.userId = this.userData.authId;
    body.product_description = this.productInfo.product_description;
    if (body.activeProductDocId) {
      this.crudService.updateActiveProducts(body.activeProductDocId, body).then(resp => {

      });
    } else {
      this.crudService.manageActiveProduct(this.productInfo.productDocId, this.userData.authId).then(resp => {
        if (resp) {
          this.crudService.addActiveProduct(body).then(resp => {
          });
        }
      });
    }
    this.productService.setProductUser(this.productInfo.productDocId, this.userData.authId, this.productBody(item, response)).then(resp => {
      this.getProductData();
      this.productService.products$.next('change');
    })
    // if (this.productInfo.users) {
    //   const findUserIndex = this.productInfo.users.findIndex((item) => item.userId === this.userData.authId);
    //   if (findUserIndex > -1) {
    //     const userData = this.productInfo.users[findUserIndex];
    //     if (response.razorpay_payment_id) {
    //       this.productInfo.users[findUserIndex].razorpay_payment_id = response.razorpay_payment_id;
    //     }
    //     if (response.session_id) {
    //       this.productInfo.users[findUserIndex].session_id = response.session_id;
    //     }
    //     if (response.subscription_id) {
    //       this.productInfo.users[findUserIndex].subscription_id = response.subscription_id;
    //     }
    //     if (item.subscription_id) {
    //       this.productInfo.users[findUserIndex].subscription_id = item.subscription_id;
    //     }
    //     this.productInfo.users[findUserIndex].paid_user = true;
    //     this.productInfo.users[findUserIndex].start_date = moment().valueOf();
    //     this.productInfo.users[findUserIndex].plan = item.plan;
    //     this.productInfo.users[findUserIndex].plan_id = item.plan_id;
    //     this.productInfo.users[findUserIndex].no_user = item.no_user ? item.no_user : '';

    //     this.productService.updateProduct(this.productInfo.productDocId, this.productInfo).then(resp => {
    //       this.getProductData();
    //     })
    //   } else {

    //     this.productInfo.users.push(this.productBody(item, response));
    //     this.productService.updateProduct(this.productInfo.productDocId, this.productInfo).then(resp => {
    //       this.getProductData();
    //     })
    //   }
    // } else {
    //   this.productInfo.users = [];
    //   this.productInfo.users.push(this.productBody(item, response));
    //   this.productService.updateProduct(this.productInfo.productDocId, this.productInfo).then(resp => {
    //     this.getProductData();
    //   })
    // }

  }



  productBody(item, response) {
    const body = {
      userId: this.userData.authId,
      paid_user: true,
      plan: item.plan,
      start_date: moment().format(),
      no_user: item.no_user ? item.no_user : ''
    } as any;
    if (item.plan_id) {
      body.plan_id = item.plan_id
    }
    if (response.razorpay_payment_id) {
      body.razorpay_payment_id = response.razorpay_payment_id;
    }
    if (response.session_id) {
      body.session_id = response.session_id;
    }
    if (response.end_date) {
      body.end_date = moment(response.end_date).format();
    }
    if (item.end_date) {
      body.end_date = item.end_date;
    }
    if (response.subscription_id) {
      body.subscription_id = response.subscription_id;
    }
    if (item.subscription_id) {
      body.subscription_id = item.subscription_id;
    }
    return body;
  }

  async openLink() {
    if (this.productInfo.cover && this.productInfo.cover.cover_button_link) {
      await Browser.open({ url: this.productInfo.cover.cover_button_link });
    }
  }


}
