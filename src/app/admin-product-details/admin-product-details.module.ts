import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminProductDetailsPageRoutingModule } from './admin-product-details-routing.module';

import { AdminProductDetailsPage } from './admin-product-details.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    AdminProductDetailsPageRoutingModule
  ],
  declarations: [AdminProductDetailsPage]
})
export class AdminProductDetailsPageModule { }
