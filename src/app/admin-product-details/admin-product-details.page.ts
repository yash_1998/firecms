import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router, ActivatedRoute } from '@angular/router';
import { FeatureService } from '../service/feature.service';
import { CommonService } from '../common.service';
import { Plugins } from '@capacitor/core';
const { Browser } = Plugins;
import * as moment from 'moment';
import { Storage } from '@ionic/storage';

import { ProductService } from '../service/product.service';
import { PolicyService } from '../service/policy.service';
import { CrudService } from '../service/crud.service';


@Component({
  selector: 'app-admin-product-details',
  templateUrl: './admin-product-details.page.html',
  styleUrls: ['./admin-product-details.page.scss'],
})
export class AdminProductDetailsPage implements OnInit {
  adminUserId: any;
  productId: any;
  productData = {} as any;
  endDate = new Date().toISOString();
  maxData: any = (new Date()).getFullYear() + 100;
  constructor(
    public crudService: CrudService,
    public router: Router, private storage: Storage, public route: ActivatedRoute, public featureService: FeatureService,
    public commonService: CommonService, public productService: ProductService, public policyService: PolicyService) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log(params)
      this.adminUserId = this.route.snapshot.params.adminId;
      this.productId = this.route.snapshot.params.productId;
      this.crudService.getActiveProductData(this.productId).then(snap => {
        snap.forEach(doc => {
          const docData = { ...doc.data(), activeDocId: doc.id } as any;
          if (docData.userId === this.adminUserId) {
            this.productData = docData;
            this.endDate = moment(docData.end_date).toISOString();
          }
        })
        console.log(this.productData);
      })
    })
  }

  updateData() {
    this.productData.end_date = moment(this.endDate).format();
    this.crudService.showLoader();
    this.crudService.updateActiveProducts(this.productData.activeDocId, this.productData).then(update => {
      this.productService.updateProductUser(this.productId, this.adminUserId, { end_date: moment(this.endDate).format() }).then(save => {
        this.crudService.showToast(`Data saved`);
        this.crudService.hideLoader();
      }).catch(e => {
        this.crudService.hideLoader();
      });
    }).catch(e => {
      this.crudService.hideLoader();
    })
  }

}
