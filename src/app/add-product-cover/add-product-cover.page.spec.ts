import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddProductCoverPage } from './add-product-cover.page';

describe('AddProductCoverPage', () => {
  let component: AddProductCoverPage;
  let fixture: ComponentFixture<AddProductCoverPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddProductCoverPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddProductCoverPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
