import { Component, OnInit } from '@angular/core';
import { UploadService } from '../service/upload.service';
import { ActivatedRoute } from '@angular/router';
import { ProductService } from '../service/product.service';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-add-product-cover',
  templateUrl: './add-product-cover.page.html',
  styleUrls: ['./add-product-cover.page.scss'],
})
export class AddProductCoverPage implements OnInit {
  fileObject;
  selectedCover = 'cover 1';
  coverObject = {} as any;
  cover2Object = {} as any;
  productId: any;
  productObject = {} as any;
  constructor(public uploadService: UploadService, public commonService: CommonService, public route: ActivatedRoute, public productService: ProductService) { }

  ngOnInit() {
    this.route.params.subscribe(event => {
      if (event) {
        this.productId = event.id;
        this.productService.getProductsById(this.productId).subscribe((resp: any) => {
          this.productObject = resp;
          if (resp.cover) {
            this.coverObject = resp.cover;
          }
          if (resp.cover2) {
            this.cover2Object = resp.cover2;
          }
          console.log(resp);
        });
      }
    });
  }

  imageUpload(event) {
    if (event.target.files[0]) {
      const file: File = event.target.files[0];
      this.fileObject = event.target.files[0];
      this.uploadService.ImageUpload(this.fileObject, ('Images/cover1/' + new Date().getTime())).then(downLoadUrl => {
        this.coverObject.url = downLoadUrl;
      });
    } else {
      // this.alertService.dismissLoader();
    }
  }

  imageUpload2(event) {
    if (event.target.files[0]) {
      const file: File = event.target.files[0];
      this.fileObject = event.target.files[0];
      this.uploadService.ImageUpload(this.fileObject, ('Images/cover2/' + new Date().getTime())).then(downLoadUrl => {
        this.cover2Object.url = downLoadUrl;
      });
    } else {
      // this.alertService.dismissLoader();
    }
  }

  setCover() {
    this.productObject.cover = this.coverObject;
    this.productService.updateProduct(this.productId, this.productObject).then(resp => {
      this.commonService.showAlert('Product cover set successfully');
    })
  } 

  setCover2() {
    this.productObject.cover2 = this.cover2Object;
    this.productService.updateProduct(this.productId, this.productObject).then(resp => {
      this.commonService.showAlert('Product cover set successfully');
    })
  }
}
