import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddProductCoverPage } from './add-product-cover.page';

const routes: Routes = [
  {
    path: '',
    component: AddProductCoverPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddProductCoverPageRoutingModule {}
