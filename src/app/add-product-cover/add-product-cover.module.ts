import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddProductCoverPageRoutingModule } from './add-product-cover-routing.module';

import { AddProductCoverPage } from './add-product-cover.page';
import { ComponentsModule } from '../components/components.module';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    AngularEditorModule,
    AddProductCoverPageRoutingModule
  ],
  declarations: [AddProductCoverPage]
})
export class AddProductCoverPageModule { }
