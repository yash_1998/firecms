import { Component, OnInit } from '@angular/core';
import { CrudService } from '../service/crud.service';
import { NavigationStart, Router } from '@angular/router';
import { CommonService } from '../common.service';
import { PolicyService } from '../service/policy.service';

@Component({
  selector: 'app-home-management',
  templateUrl: './home-management.page.html',
  styleUrls: ['./home-management.page.scss'],
})
export class HomeManagementPage implements OnInit {
  footer1Object = {
    id: 'footer1'
  } as any;
  students: any;
  studentName: string;
  studentAge: number;
  studentAddress: string;
  coverObject = {} as any;
  constructor(private crudService: CrudService, public router: Router, public policyService: PolicyService, public commonService: CommonService) { }


  ngOnInit() {
    this.policyService.getExtraInfoId('footer1').subscribe(resp => {
      if (resp) {
        this.footer1Object = resp;
      }

    });
    this.getCoverData();
    this.router.events.subscribe((event: NavigationStart) => {
      if (event instanceof NavigationStart) {
        if (event.url === '/tabs/contacts') {
          // this.getCoverData();
          // this.segmentChanged(null, this.selectedSegment);
        }
      }
    });
  }


  getCoverData() {
    this.crudService.getNewCover().subscribe(data => {
      //  data.doc
      data.map(e => {
        this.coverObject = e;
      });
      if (data.length === 0) {
        this.coverObject = {};
      }
    });
  }

  CreateCover() {

    this.setFooter1();
    this.crudService.create_NewCover(this.coverObject).then(resp => {
      this.getCoverData();
      this.commonService.showAlert('New cover added');
      this.crudService.updateNewCover(resp.id, { ... this.coverObject, coverId: resp.id });
    });
    // let record = {};
    // record['Name'] = this.studentName;
    // record['Age'] = this.studentAge;
    // record['Address'] = this.studentAddress;
    // this.crudService.create_NewStudent(record).then(resp => {
    //   this.studentName = "";
    //   this.studentAge = undefined;
    //   this.studentAddress = "";
    //   console.log(resp);
    // })
    //   .catch(error => {
    //     console.log(error);
    //   });
  }

  setFooter1() {
    this.policyService.setExtraInfo('footer1', this.footer1Object).then(resp => {

    });
  }


  goToPolicy() {
    this.router.navigate(['/add-privacy']);
  }

  goToHowItWork() {
    this.router.navigate(['/add-how-it-works']);
  }

  goToAboutUs() {
    this.router.navigate(['/about-us']);
  }

  goToFooter1() {
    this.router.navigate(['/add-footer1']);
  }

  goToFooter2() {
    this.router.navigate(['/add-footer2']);
  }

  RemoveRecord() {
    this.footer1Object = {
      id: 'footer1'
    } as any;
    this.setFooter1();
    this.crudService.deleteNewCover(this.coverObject.coverId).then(resp => {
      this.getCoverData();
      this.commonService.showAlert('Cover removed');
    });
  }

  EditRecord(record) {
    // record.isEdit = true;
    // record.EditName = record.Name;
    // record.EditAge = record.Age;
    // record.EditAddress = record.Address;
  }

  UpdateRecord() {
    this.setFooter1()
    this.crudService.updateNewCover(this.coverObject.coverId, this.coverObject).then(resp => {
      this.commonService.showAlert('Cover updated');
      this.getCoverData();
    });
  }

}
