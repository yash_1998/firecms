import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewLeadsPageRoutingModule } from './view-leads-routing.module';

import { ViewLeadsPage } from './view-leads.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ViewLeadsPageRoutingModule
  ],
  declarations: [ViewLeadsPage]
})
export class ViewLeadsPageModule { }
