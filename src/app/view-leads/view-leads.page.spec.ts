import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ViewLeadsPage } from './view-leads.page';

describe('ViewLeadsPage', () => {
  let component: ViewLeadsPage;
  let fixture: ComponentFixture<ViewLeadsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewLeadsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ViewLeadsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
