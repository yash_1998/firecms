import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewLeadsPage } from './view-leads.page';

const routes: Routes = [
  {
    path: '',
    component: ViewLeadsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewLeadsPageRoutingModule {}
