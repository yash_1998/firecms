import { Component, OnInit } from '@angular/core';
import { CrudService } from '../service/crud.service';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { ProductService } from '../service/product.service';
@Component({
  selector: 'app-view-leads',
  templateUrl: './view-leads.page.html',
  styleUrls: ['./view-leads.page.scss'],
})
export class ViewLeadsPage implements OnInit {
  userId = '';
  users = [];
  leads = [];
  selectedValue = 'open';
  selectedUser = {};
  options = ['open', 'converted', 'closed'];
  constructor(
    public productService: ProductService,
    public router: Router, public crudService: CrudService, private storage: Storage, ) { }

  ngOnInit() {
    this.storage.get('loggedInUser').then((val) => {
      this.userId = val
      console.log('L-Your user ID is', val);
      if (val != null || "") {
        console.log("L-some one is logged in");

        //  this.navCtrl.navigateRoot('/dashboard');
      } else {
        console.log("L-no one is logged in");
      }
    });
    this.crudService.getUsers().subscribe(async resp => {
      this.users = [];
      resp.forEach((actions) => {
        const body: any = actions.payload.doc.data();
        this.users.push(body);
      })
      console.log(this.users);
      // this.leads = resp;
      // this.leads.sort((a, b) => parseInt(a.orderby) - parseInt(b.orderby));
    });

  }

  // getUserLeads() {
  //   this.crudService.getUserLeads()
  // }

  chageUser(event) {
    console.log(event.target.value);
    if (event.target.value) {
      this.leads = [];
      this.crudService.getUserLeads(event.target.value.authId).then(resp => {
        resp.forEach(doc => {
          this.leads.push(doc.data());
        })
      })
      console.log(this.leads);
    }

  }

  editLead(leadDocId) {
    this.router.navigate(['edit-lead', leadDocId]);
  }

  goToLeadChat(leadDocId) {
    this.router.navigate(['mylead-chat', leadDocId]);
  }

  deleteLead(id) {
    this.productService.deleteLead(id).then(resp => {

    })
  }


}
