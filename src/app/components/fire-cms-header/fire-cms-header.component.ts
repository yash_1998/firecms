import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Location } from '@angular/common';
import { PolicyService } from 'src/app/service/policy.service';
import { CommonService } from 'src/app/common.service';
import { ProductModalComponent } from 'src/app/product-modal/product-modal.component';
import { AngularFireAuth } from '@angular/fire/auth';
import { AuthenticateService } from 'src/app/service/authentication.service';
import { CrudService } from 'src/app/service/crud.service';

@Component({
  selector: 'app-fire-cms-header',
  templateUrl: './fire-cms-header.component.html',
  styleUrls: ['./fire-cms-header.component.scss'],
})
export class FireCmsHeaderComponent implements OnInit {
  public userEmail;
  public userData = {} as any;
  public logoInfo = { url: '' } as any;
  public authState: any = null;
  @Input() title: string;
  @Input() hideSideMenu;
  @Input() goBack;
  constructor(
    public router: Router,
    private storage: Storage,
    public crudService: CrudService,
    private authService: AuthenticateService,
    public commonService: CommonService,
    public policyService: PolicyService,
    public location: Location,
    private firebaseAuth: AngularFireAuth
  ) { }

  ngOnInit() {
    this.getLoginData();
    this.loadUserData();
    this.firebaseAuth.authState.subscribe(authState => {
      if (authState) {
        this.authState = authState.uid;
      } else {
        this.authState = null;
      }

    });
    this.policyService.getExtraInfoId('companylogo').subscribe(response => {

      this.logoInfo = response;

      console.log(response);
    });
    this.router.events.subscribe((event: NavigationStart) => {
      if (event instanceof NavigationStart) {
        this.getLoginData();
        if (event.url === '/home') {
          this.loadUserData();
        }
        // this.segmentChanged(null, this.selectedSegment);
      }
    });
  }

  loadUserData() {
    this.storage.get('loggedInUser').then((val) => {
      if (val) {
        this.crudService.getUserData(val).then(userData => {
          userData.forEach(doc => {
            this.userData = doc.data();
          })
        })
      }
    })
  }

  goToFreeUser() {
    this.router.navigate(['free-user-creation']);
  }


  getLoginData() {
    this.storage.get('userEmail').then((val) => {
      console.log('Your mail is', val);
      this.userEmail = val;
    });
  }

  goToLogin() {
    this.router.navigate(['login']);
  }

  goToContact() {
    this.router.navigate(['contact']);
  }

  goBackPage() {
    this.location.back();
  }


  logout() {
    console.log('clicked');
    this.authService.logoutUser()
      .then(res => {
        console.log(res);
        this.storage.set('loggedInUser', null);
        this.storage.set('userEmail', null);
        this.storage.set('userName', null);
        this.storage.set('userlName', null);
        this.storage.set('allowedPages', null);

        this.goToLogin();
      })
      .catch(error => {
        console.log(error);
      });
  }


  navigate(route) {
    this.router.navigate([route]);
  }


  openProduct() {
    this.commonService.presentModal(ProductModalComponent, {}, 'left-modal').then(resp => {

    });
  }
}
