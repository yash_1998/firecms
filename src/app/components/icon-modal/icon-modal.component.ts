import { Component, OnInit, Input } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-icon-modal',
  templateUrl: './icon-modal.component.html',
  styleUrls: ['./icon-modal.component.scss'],
})
export class IconModalComponent implements OnInit {
  @Input() iconLists;
  constructor(public modalController: ModalController) { }

  ngOnInit() {
    console.log(this.iconLists);
  }

  selectItem(item) {
    this.modalController.dismiss(item);
  }

}
