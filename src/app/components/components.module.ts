import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FireCmsHeaderComponent } from './fire-cms-header/fire-cms-header.component';
import { FormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { FireCmsFooterComponent } from './fire-cms-footer/fire-cms-footer.component';
import { IconModalComponent } from './icon-modal/icon-modal.component';



@NgModule({
  declarations: [FireCmsHeaderComponent, FireCmsFooterComponent, IconModalComponent],
  entryComponents: [IconModalComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CommonModule
  ],
  exports: [FireCmsHeaderComponent, FireCmsFooterComponent, IconModalComponent]
})
export class ComponentsModule { }
