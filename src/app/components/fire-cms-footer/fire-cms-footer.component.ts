import { Component, OnInit } from '@angular/core';
import { PolicyService } from 'src/app/service/policy.service';
import { Router, NavigationStart } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { CommonService } from 'src/app/common.service';
import { Platform } from '@ionic/angular';
const { Browser } = Plugins;
@Component({
  selector: 'app-fire-cms-footer',
  templateUrl: './fire-cms-footer.component.html',
  styleUrls: ['./fire-cms-footer.component.scss'],
})
export class FireCmsFooterComponent implements OnInit {
  footerObject = {} as any;
  constructor(public platform: Platform, public policyService: PolicyService, public router: Router, public commonService: CommonService) { }

  ngOnInit() {
    this.getPolicies();
  }


  getPolicies() {
    this.policyService.getExtraInfo().subscribe(doc => {
      doc.map((data: any) => {
        this.footerObject = {
          ...this.footerObject,
          [data.id]: data
        };
      });
    });
  }

  async openLink(url) {
    if (url) {
      await Browser.open({ url });
    }
  }

  openDialPicker(contact) {
    window.open(`tel:${contact}`, '_system');
  }
  mailto(email) {
    this.platform.ready().then(() => {
      window.open('mailto:' + email);
    });
  }

  viewPolicy(name) {
    this.router.navigate(['policy-view', name]);
  }

}
