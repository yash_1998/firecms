import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ProductService } from '../service/product.service';
import { NavigationStart, Router, ActivatedRoute } from '@angular/router';
import { FeatureService } from '../service/feature.service';
import { CommonService } from '../common.service';
import { ModalController } from '@ionic/angular';
import { IconModalComponent } from '../components/icon-modal/icon-modal.component';
import { UploadService } from '../service/upload.service';

@Component({
  selector: 'app-add-feature',
  templateUrl: './add-feature.page.html',
  styleUrls: ['./add-feature.page.scss'],
})
export class AddFeaturePage implements OnInit {
  featureObject = {} as any;
  @ViewChild('imgInput') imgInput: ElementRef;
  products = [];
  prouductId;
  index;
  selectedProduct = {} as any;
  iconLists = [
    'Custom Image',
    'body',
    'body-outline',
    'add',
    'add-outline',
    'add-circle',
    'add-circle-outline',
    'airplane',
    'airplane-outline',
    'alarm',
    'alarm-outline',
    'albums',
    'albums-outline',
    'alert',
    'alert-outline',
    'alert-circle',
    'alert-circle-outline',
    'american-football',
    'american-football-outline',
    'analytics',
    'analytics-outline',
    'aperture',
    'aperture-outline',
    'apps',
    'apps-outline',
    // 'appstore',
    // 'appstore-outline',
    'archive',
    'archive-outline',
    'arrow-back',
    'arrow-back-outline',
    'arrow-down',
    'arrow-down-outline',
    'american-football',
    'american-football-outline',
    'analytics',
    'analytics-outline',
    'aperture',
    'aperture-outline',
    'apps',
    'apps-outline',
    'archive',
    'archive-outline',
    'arrow-back',
    'arrow-back-outline',
    'arrow-back-circle',
    'arrow-back-circle-outline',
    'arrow-down',
    'arrow-down-outline',
    'book', 'search', 'battery-full-outline', 'beaker', 'beaker-outline'];
  constructor(
    private route: ActivatedRoute,
    public uploadService: UploadService,
    public productService: ProductService, public commonService: CommonService,
    public router: Router, public featureService: FeatureService, public modalController: ModalController) { }

  ngOnInit() {
    this.route.params.subscribe(event => {
      if (event) {
        this.prouductId = event.id;
        this.index = event.index;
        if (this.prouductId) {
          this.productService.getProductsById(this.prouductId).subscribe((resp: any) => {
            if (this.index !== undefined) {
              this.featureObject = resp.features[this.index];
            }
            this.selectedProduct = resp;
          });
        }
      }
    });
    this.getProductData();
    this.router.events.subscribe((event: NavigationStart) => {
      if (event instanceof NavigationStart) {
        this.getProductData();
        // this.segmentChanged(null, this.selectedSegment);
      }
    });
  }

  getProductData() {
    this.products = [];
    this.productService.getProducts().subscribe(resp => {
      this.products = [];
      resp.map(data => {
        this.products.push(data);
      });
      this.products.sort((a, b) => parseInt(a.orderby) - parseInt(b.orderby));
    });
  }


  async presentModal() {
    const modal = await this.modalController.create({
      component: IconModalComponent,
      cssClass: 'my-custom-class',
      componentProps: { iconLists: this.iconLists }
    });
    await modal.present();

    const { data } = await modal.onWillDismiss();
    if (data) {
      if (data === 'Custom Image') {
        this.imgInput.nativeElement.click();
      }
      this.featureObject.feature_icon = data;
      console.log(data);
    }
  }

  imageUpload(event) {
    if (event.target.files[0]) {
      const file: File = event.target.files[0];
      if (!this.selectedProduct.howitworks) {
        this.selectedProduct.howitworks = [];
      }
      this.uploadService.ImageUpload(file, ('Images/' + new Date().getTime() + '/')).then(downLoadUrl => {
        this.featureObject.url = downLoadUrl;
      });
    } else {
      // this.alertService.dismissLoader();
    }
  }


  addFeature() {
    if (this.selectedProduct && this.selectedProduct.productDocId) {
      if (!this.selectedProduct.features) {
        this.selectedProduct.features = [];
        this.selectedProduct.features.push(this.featureObject);
      } else {
        if (this.index === undefined) {
          this.selectedProduct.features.push(this.featureObject);
        } else {
          this.selectedProduct.features[this.index] = this.featureObject;
        }

      }
      this.productService.updateProduct(this.selectedProduct.productDocId, this.selectedProduct).then(resp => {
        if (this.prouductId && this.index !== undefined) {
          this.commonService.showAlert('Feature updated');
        } else {
          this.featureObject = {};
          this.commonService.showAlert('New feature added for selected product');
        }

      });
    }
  }

}
