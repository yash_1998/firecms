import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddFeaturePageRoutingModule } from './add-feature-routing.module';

import { AddFeaturePage } from './add-feature.page';
import { ComponentsModule } from '../components/components.module';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    AngularEditorModule,
    AddFeaturePageRoutingModule
  ],
  declarations: [AddFeaturePage]
})
export class AddFeaturePageModule { }
