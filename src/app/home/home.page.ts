import { Component, OnInit } from '@angular/core';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Contacts, Contact, ContactField, ContactName, ContactFindOptions, ContactFieldType } from '@ionic-native/contacts';

//for modal
import { ModalController, AlertController } from '@ionic/angular';
import { NavigationStart, Router, NavigationExtras } from '@angular/router';
import { CrudService } from '../service/crud.service';
import { ProductService } from '../service/product.service';
import { PolicyService } from '../service/policy.service';
import { CommonService } from '../common.service';
import { Storage } from '@ionic/storage';
import { Title } from '@angular/platform-browser';
import * as moment from 'moment';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  browserObject = {} as any;
  coverObject = {} as any;
  userData = {} as any;
  products = [];
  constructor(
    public callNumber: CallNumber, public contacts: Contacts, public modalController: ModalController,
    public crudService: CrudService, public router: Router, private storage: Storage, public prouductService: ProductService,
    public policyService: PolicyService, public commonService: CommonService, public titleService: Title) {


  }
  ngOnInit() {
    this.policyService.getExtraInfoId('browser_title').subscribe((resp: any) => {
      if (resp) {
        this.browserObject = resp;
        if (resp.browser_title) {
          this.titleService.setTitle(resp.browser_title);
        }
      }

    });

    this.policyService.getExtraInfoId('primaryColor').subscribe((resp: any) => {
      console.log(resp);
      if (resp) {
        this.commonService.primaryColor = resp.color;
      } else {
        this.commonService.primaryColor = '#8c8cf9';
        this.policyService.setExtraInfo('primaryColor', { id: 'primaryColor', color: '#8c8cf9' });
      }
    });
    this.getCoverData();
    this.storage.get('loggedInUser').then((val) => {
      if (val) {
        this.crudService.getUserData(val).then(userData => {
          console.log(userData);

          userData.forEach(doc => {
            this.userData = doc.data();
            console.log(doc.data());
          })
          this.crudService.updateNewStudents(val, { last_login: moment().format() }).then(resp => {

          })
          this.getProductData();
        })
      } else {
        this.getProductData();
      }
    });

    this.router.events.subscribe((event: NavigationStart) => {
      if (event instanceof NavigationStart) {
        this.getCoverData();
        // this.getProductData();
        // this.segmentChanged(null, this.selectedSegment);
      }
    });
  }


  getProductData() {
    this.products = [];
    this.prouductService.getProducts().subscribe(resp => {
      this.products = [];
      console.log(resp);
      resp.map((data: any) => {
        if (data.status === 'active') {
          this.products.push(data);
        }
      });
      let count = 0;
      for (let i = 0; i < this.products.length; i++) {
        const productData = this.products[i];
        this.prouductService.getProductUser(productData.productDocId, this.userData.authId).then(userData => {
          const userProductData = userData.data();
          if (userProductData) {
            this.products[count].paid_user = userProductData.paid_user;
            this.products[count].end_date = userProductData.end_date;
            this.products[count].start_date = userProductData.start_date;
            this.products[count] = { ...this.products[count], ...userProductData }
          }
          count++;
          if (count === (this.products.length - 1)) {
            this.products.sort((a, b) => parseInt(a.orderby) - parseInt(b.orderby));
          }
        })
      }
    });
  }


  getCoverData() {
    this.crudService.getNewCover().subscribe(data => {
      //  data.doc
      data.map(e => {
        this.coverObject = e;
      });
      if (data.length === 0) {
        this.coverObject = {};
      }
    });
  }


  getNumberOfDays(data) {
    const endDate = moment(data.end_date).valueOf();
    const todayDate = moment().valueOf();
    return (this.diffDays(endDate, todayDate));
  }

  diffDays(d1, d2) {
    var ndays;

    ndays = (d2 - d1) / 1000 / 86400;
    ndays = Math.round(ndays - 0.5);
    return Math.abs(ndays);
  };



  goToProductInfo(item) {
    const navigationExtras: NavigationExtras = { state: { productInfo: item } };
    this.router.navigate(['product-detail', item.productDocId], navigationExtras);
  }


  freeProudct(product) {
    const productBody = {} as any;
    const start = moment().format();
    const end = moment().add(14, 'd').format();
    productBody.userId = this.userData.authId;
    productBody.start_date = start;
    productBody.end_date = end;
    productBody.paid_user = false;
    const activeProductBody = {} as any;
    activeProductBody.start_date = start;
    activeProductBody.end_date = end;
    activeProductBody.productDocId = product.productDocId;
    activeProductBody.product_name = product.product_name;
    activeProductBody.product_image = product.product_image;
    activeProductBody.prices = product.prices;
    activeProductBody.features = product.features;
    activeProductBody.userId = this.userData.authId;
    activeProductBody.product_description = product.product_description;
    activeProductBody.paid_user = false;
    this.prouductService.setProductUser(product.productDocId, this.userData.authId, productBody).then(resp => {
      this.crudService.showToast(`Free trial started for ${product.product_name}`);
      this.getProductData();
    })
    this.crudService.manageActiveProduct(product.productDocId, this.userData.authId).then(resp => {
      if (resp) {
        this.crudService.addActiveProduct(activeProductBody).then(resp => {
          this.crudService.updateActiveProducts(resp.id, { activeProductDocId: resp.id, ...activeProductBody }).then(resp => {
          });
        });
      }
    });
  }

}
