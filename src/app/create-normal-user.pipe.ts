import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'createNormalUser'
})
export class CreateNormalUserPipe implements PipeTransform {

  transform(value: unknown, ...args: unknown[]): unknown {
    return null;
  }

}
