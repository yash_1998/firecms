import { Component, OnInit } from '@angular/core';
import { PolicyService } from '../service/policy.service';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-view-about-us',
  templateUrl: './view-about-us.page.html',
  styleUrls: ['./view-about-us.page.scss'],
})
export class ViewAboutUsPage implements OnInit {
  aboutusObject = {} as any;
  constructor(public policyService: PolicyService, public commonService: CommonService) { }

  ngOnInit() {
    this.policyService.getExtraInfoId('aboutus').subscribe(resp => {
      if (resp) {
        this.aboutusObject = resp;
      }
    })
  }

}
