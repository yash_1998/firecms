import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ViewAboutUsPageRoutingModule } from './view-about-us-routing.module';

import { ViewAboutUsPage } from './view-about-us.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ViewAboutUsPageRoutingModule
  ],
  declarations: [ViewAboutUsPage]
})
export class ViewAboutUsPageModule { }
