import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ViewAboutUsPage } from './view-about-us.page';

const routes: Routes = [
  {
    path: '',
    component: ViewAboutUsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ViewAboutUsPageRoutingModule {}
