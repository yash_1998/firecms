import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { NavigationExtras, Router } from '@angular/router';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-product-modal',
  templateUrl: './product-modal.component.html',
  styleUrls: ['./product-modal.component.scss'],
})
export class ProductModalComponent implements OnInit {
  products = [];
  constructor(public prouductService: ProductService, public router: Router, public modalController: ModalController) { }

  ngOnInit() {
    this.prouductService.getProducts().subscribe(resp => {
      this.products = [];
      resp.map((data: any) => {
        if (data.status === 'active') {
          this.products.push(data);
        }
      });
      this.products.sort((a, b) => parseInt(a.orderby) - parseInt(b.orderby));
    });
  }

  goToProductInfo(item) {
    this.modalController.dismiss();
    const navigationExtras: NavigationExtras = { state: { productInfo: item } };
    this.router.navigate(['product-detail', item.productDocId], navigationExtras);
  }

}
