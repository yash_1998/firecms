import { Component, OnInit } from '@angular/core';

import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import * as firebase from 'firebase/app';
import { environment } from '../environments/environment';
import { CommonService } from './common.service';
import { PolicyService } from './service/policy.service';
import { AngularFireAuth } from '@angular/fire/auth';
import { ProductModalComponent } from './product-modal/product-modal.component';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  public selectedIndex = 0;
  public authState: any = null;
  public appPages = [
    {
      title: 'DashBoard',
      url: '/dashboard',
    },
    {
      title: 'Home',
      url: '/home'
    },
    {
      title: 'Products',
      url: ''
    },
    {
      title: 'Contact',
      url: '/contact'
    },
    {
      title: 'About Us',
      url: '/view-about-us'
    },
    // {
    //   title: 'Company Settings',
    //   url: '/company-setting'
    // }
    //    {
    //   title: 'Call',
    //   url: '/home',
    //   icon: 'mail'
    // },
    // {
    //   title: 'Logs Today',
    //   url: '/logs',
    //   icon: 'mail'
    // },
    // {
    //   title: 'View Call Register',
    //   url: '/viewcalllogs',
    //   icon: 'mail'
    // },
    // {
    //   title: 'Start Round',
    //   url: '/gscan',
    //   icon: 'mail'
    // },

    // {
    //   // kranti meter service agent
    //   title: 'Service Agent',
    //   url: '/serviceagententry',
    //   icon: 'mail'
    // },

    // {
    //   // kranti meter service agent
    //   title: 'Service Agent',
    //   url: '/serviceagent-pshiba',
    //   icon: 'mail'
    // },
    // {
    //   // kranti meter service agent
    //   title: 'View Open Complaints',
    //   url: '/viewcomplaints',
    //   icon: 'mail'
    // },
    // {
    //   // kranti meter service agent
    //   title: 'View Closed Complaints',
    //   url: '/viewclosedcomplaints',
    //   icon: 'mail'
    // },
    //{
    // kranti meter service agent
    //   title: 'Service Agent2',
    //   url: '/serviceagent-pshiba2',
    //   icon: 'mail'
    // },
    // {
    //   title: 'Supervisor Log Visit',
    //   url: '/scan',
    //   icon: 'mail'
    // needed for yogesh supervisor visit with sms
    // },


    // {
    //   title: 'Logs Yesterday',
    //   url: '/logsyesterday',
    //   icon: 'mail'
    // },
    // {
    //   title: 'Dial',
    //   url: '/dial',
    //   icon: 'mail'
    // },
    // {
    //   title: 'Contacts',
    //   url: '/contact',
    //   icon: 'mail'
    // },
    // {
    //   title: 'Inbox',
    //   url: '/folder/Inbox',
    //   icon: 'mail'
    // },
    // {
    //   title: 'Outbox',
    //   url: '/folder/Outbox',
    //   icon: 'paper-plane'
    // },
    // {
    //   title: 'Favorites',
    //   url: '/folder/Favorites',
    //   icon: 'heart'
    // },
    // {
    //   title: 'Archived',
    //   url: '/folder/Archived',
    //   icon: 'archive'
    // },
    // {
    //   title: 'Trash',
    //   url: '/folder/Trash',
    //   icon: 'trash'
    // },
    // {
    //   title: 'Spam',
    //   url: '/folder/Spam',
    //   icon: 'warning'
    // }
  ];
  // public labels = ['Family', 'Friends', 'Notes', 'Work', 'Travel', 'Reminders'];

  constructor(
    private platform: Platform,
    private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    public router: Router,
    private firebaseAuth: AngularFireAuth,
    public commonService: CommonService,
  ) {
    this.initializeApp();
  }

  initializeApp() {
    firebase.initializeApp(environment.firebaseConfig);
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openProduct() {
    this.commonService.presentModal(ProductModalComponent, {}, 'left-modal').then(resp => {

    });
  }

  navigation(p) {
    if (p.url) {
      this.router.navigate([p.url]);
    } else {
      this.openProduct();
    }
  }

  ngOnInit() {
    firebase.firestore().enablePersistence().then(resp => {
      console.log(resp);
    }).catch(error => {
      console.log(error);
    })
    this.firebaseAuth.authState.subscribe(authState => {
      if (authState) {
        this.authState = authState.uid;
      } else {
        this.authState = null;
      }

    });
    // this.commonService.primaryColor = '#8c8cf9';
    // this.policyService.getExtraInfoId('primaryColor').subscribe((resp) => {
    //   console.log(resp);
    //   if (resp) {
    //     // \this.commonService.primaryColor = resp.hex;
    //   } else {
    //     // this.commonService.primaryColor = '#8c8cf9';
    //     // this.commonService.setObject('primaryColor', { hex: '#8c8cf9' });
    //   }
    // })
    // const path = window.location.pathname.split('folder/')[1];
    // if (path !== undefined) {
    //   this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    //}
  }
}
