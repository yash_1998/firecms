import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddFooter2Page } from './add-footer2.page';

const routes: Routes = [
  {
    path: '',
    component: AddFooter2Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddFooter2PageRoutingModule {}
