import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddFooter2PageRoutingModule } from './add-footer2-routing.module';

import { AddFooter2Page } from './add-footer2.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    AddFooter2PageRoutingModule
  ],
  declarations: [AddFooter2Page]
})
export class AddFooter2PageModule { }
