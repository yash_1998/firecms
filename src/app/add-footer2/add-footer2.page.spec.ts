import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddFooter2Page } from './add-footer2.page';

describe('AddFooter2Page', () => {
  let component: AddFooter2Page;
  let fixture: ComponentFixture<AddFooter2Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFooter2Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddFooter2Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
