import { Component, OnInit } from '@angular/core';
import { PolicyService } from '../service/policy.service';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-add-footer2',
  templateUrl: './add-footer2.page.html',
  styleUrls: ['./add-footer2.page.scss'],
})
export class AddFooter2Page implements OnInit {
  footer2Object = {
    id: 'footer2'
  } as any;
  constructor(public policyService: PolicyService, public commonService: CommonService) { }

  ngOnInit() {
    this.policyService.getExtraInfoId('footer2').subscribe(resp => {
      this.footer2Object = resp;
    })
  }

  setFooter2() {
    this.policyService.setExtraInfo('footer2', this.footer2Object).then(resp => {
      this.commonService.showAlert('Footer 2 updated');
    });
  }

}
