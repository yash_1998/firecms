import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PolicyViewPage } from './policy-view.page';

describe('PolicyViewPage', () => {
  let component: PolicyViewPage;
  let fixture: ComponentFixture<PolicyViewPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PolicyViewPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PolicyViewPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
