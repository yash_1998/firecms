import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PolicyViewPageRoutingModule } from './policy-view-routing.module';

import { PolicyViewPage } from './policy-view.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    PolicyViewPageRoutingModule
  ],
  declarations: [PolicyViewPage]
})
export class PolicyViewPageModule { }
