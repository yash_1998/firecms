import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { PolicyService } from '../service/policy.service';

@Component({
  selector: 'app-policy-view',
  templateUrl: './policy-view.page.html',
  styleUrls: ['./policy-view.page.scss'],
})
export class PolicyViewPage implements OnInit {
  policyId = '';
  policyInfo = {} as any;
  constructor(public route: ActivatedRoute, public router: Router, public policyService: PolicyService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      this.policyId = this.route.snapshot.params.id;
      this.policyService.getExtraInfoId(this.policyId).subscribe(resp => {
        this.policyInfo = resp;
      });
    });
  }

}
