import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PolicyViewPage } from './policy-view.page';

const routes: Routes = [
  {
    path: '',
    component: PolicyViewPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PolicyViewPageRoutingModule {}
