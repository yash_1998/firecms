import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { UploadService } from '../service/upload.service';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-add-product',
  templateUrl: './add-product.page.html',
  styleUrls: ['./add-product.page.scss'],
})
export class AddProductPage implements OnInit {
  productObject = {} as any;
  productId;
  fileObject;
  url = '';
  constructor(
    public productService: ProductService,
    public commonService: CommonService,
    private route: ActivatedRoute,
    public router: Router, public uploadService: UploadService) { }

  ngOnInit() {
    this.route.params.subscribe(event => {
      if (event && event.id) {
        this.productId = event.id;
        this.productService.getProductsById(this.productId).subscribe(resp => {
          this.productObject = resp;
          console.log(resp);
        });
      }
      console.log(event);
    });
  }


  deletePrice(i) {
    this.productObject.prices.splice(i, 1);
    this.productService.updateProduct(this.productId, this.productObject).
      then(update => {

      });
  }

  deleteFeature(i) {
    this.productObject.features.splice(i, 1);
    this.productService.updateProduct(this.productId, this.productObject).
      then(update => {

      });
  }

  deleteHowItWorks(i) {
    this.productObject.howitworks.splice(i, 1);
    this.productService.updateProduct(this.productId, this.productObject).
      then(update => {

      });
  }

  editPrice(i) {
    this.router.navigate(['edit-price', this.productId, i]);
  }


  editFeature(i) {
    this.router.navigate(['edit-feature', this.productId, i]);
  }

  editHowItWorks(i) {
    this.router.navigate(['edit-how-it-works', this.productId, i]);
  }

  addProduct() {
    this.productService.addProuduct(this.productObject).then(resp => {
      this.uploadService.ImageUpload(this.fileObject, ('Images/' + resp.id + '/')).then(downLoadUrl => {
        this.commonService.showAlert('New Product Added');
        this.productService.createStripeProduct({ ...this.productObject, product_image: downLoadUrl }).subscribe(productResp => {
          const parseData = JSON.parse(productResp.data);
          this.productService.updateProduct(resp.id, { ...this.productObject, stripe_product_id: parseData.id, productDocId: resp.id, product_image: downLoadUrl }).
            then(update => {
              this.productObject = {};
            });
        })
      });
    });
  }


  updateProduct() {
    if (this.fileObject) {
      this.uploadService.ImageUpload(this.fileObject, ('Images/' + this.productId + '/')).then(downLoadUrl => {
        this.productService.updateProduct(this.productId, {
          ...this.productObject, productDocId: this.productId,
          product_image: downLoadUrl
        }).
          then(update => {
            this.productService.updateStripeProduct(this.productObject).subscribe(resp => {

            });
          });
      });
    } else {
      this.productService.createStripeProduct(this.productObject).subscribe(resp => {
        const parseData = JSON.parse(resp.data);
        this.productObject.stripe_product_id = parseData.id;
        this.productService.updateProduct(this.productId, this.productObject).then(resp => {
          // this.productService.updateStripeProduct(this.productObject).subscribe(resp => {

          // });
          // this.uploadService.ImageUpload(this.fileObject, ('Images/' + resp.id + '/')).then(downLoadUrl => {
          this.commonService.showAlert('Product Updated');
          // this.productService.updateProduct(resp.id, { ...this.productObject, productDocId: resp.id, product_image: downLoadUrl }).
          //   then(update => {

          //   });
          // });
        });
      })

    }
  }

  goToFeature() {
    this.router.navigate(['add-feature', this.productId]);
  }

  goToHowItWork() {
    this.router.navigate(['/add-how-it-works', this.productId]);
  }


  goToPricing() {
    this.router.navigate(['add-price', this.productId]);
  }


  imageUpload(event) {
    if (event.target.files[0]) {
      this.readUrl(event)
      const file: File = event.target.files[0];
      this.fileObject = event.target.files[0];
    } else {
      // this.alertService.dismissLoader();
    }
  }

  readUrl(event: any) {
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();

      reader.onload = (data: any) => {
        this.url = data.target.result;
      }

      reader.readAsDataURL(event.target.files[0]);
    }
  }



}
