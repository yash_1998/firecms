import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { ProductService } from '../service/product.service';
import { CommonService } from '../common.service';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase/app';
import { Plugins } from '@capacitor/core';

const { Clipboard } = Plugins;
import { CrudService } from '../service/crud.service';
@Component({
  selector: 'app-manage',
  templateUrl: './manage.page.html',
  styleUrls: ['./manage.page.scss'],
})
export class ManagePage implements OnInit {
  prouductId;
  index;
  featuresArray = [{}];
  users = [];
  userData = {} as any;
  restrictData = {} as any;
  selectedProduct = {} as any;

  constructor(
    private storage: Storage, public crudService: CrudService,
    public router: Router, public productService: ProductService,
    public commonService: CommonService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(event => {
      if (event) {
        this.prouductId = event.id;
        this.index = event.index;
        if (this.prouductId) {
          this.productService.getProductsById(this.prouductId).subscribe((resp: any) => {
            this.selectedProduct = resp;
          });
        }
      }
    });




    this.getUser();
    this.router.events.subscribe((event: NavigationStart) => {
      if (event instanceof NavigationStart) {
        this.getUser();
      }
    });
  }

  goToCreateUser() {
    if ((this.users.length - 1) < parseInt(this.restrictData.no_user)) {
      this.router.navigate(['create-normal-user']);
    } else {
      if (!this.restrictData.paid_user && ((this.users.length - 1) == 0)) {
        this.router.navigate(['create-normal-user']);
      } else if (!this.restrictData.paid_user && ((this.users.length - 1) > 0)) {
        this.crudService.showToast('you reach to maximum limit of user and max limit is 1');
      }
      if (this.restrictData.no_user) {
        this.crudService.showToast('you reach to maximum limit of user and max limit is ' + this.restrictData.no_user);
      }

    }
  }


  goTo

  copyLink(link) {
    Clipboard.write({
      string: link,
    });
    window.open(link, '_blank');
    this.commonService.showAlert('link copied');
  }


  deleteUser(item) {
    firebase.auth().signInWithEmailAndPassword(item.email, item.password).then(resp => {
      if (resp) {
        resp.user.delete();
        this.crudService.removeStudent(item.authId).then(resp => {
          firebase.auth().signInWithEmailAndPassword(this.userData.email, this.userData.password).then(resp => {
            this.getUser();
          })
        });

      }
    })
  }

  getUser() {
    this.storage.get('loggedInUser').then((val) => {
      if (val) {
        this.productService.getProductsById(this.prouductId).subscribe((resp: any) => {
          this.productService.getProductUser(this.prouductId, val).then(snap => {
            if (snap.data()) {
              this.restrictData = snap.data();
            }
          });
        })
        this.crudService.getUserData(val).then(userData => {
          console.log(userData);
          userData.forEach(doc => {
            this.userData = doc.data();
            console.log(doc.data());
          })
          this.crudService.getUsrByCompanyName(this.userData.company_code).then((res: any) => {
            if (res) {
              this.users = [];
              res.forEach(doc => {
                this.users.push(doc.data())
                console.log(doc.data());
              })
            }
            console.log(this.users);
          })
        })
      }
    })
  }


  editUser(item) {
    this.router.navigate(['edit-normal-user', item.authId]);
  }

  goToAdminSttings() {
    this.router.navigate(['admin-settings', this.prouductId]);
  }
}
