import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../service/product.service';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-add-price',
  templateUrl: './add-price.page.html',
  styleUrls: ['./add-price.page.scss'],
})
export class AddPricePage implements OnInit {
  pricingObject = {
    featuresArray: [],
    currency: 'INR'
  } as any;
  products = [];
  currencyOptions = ['INR', 'USD'];
  public periods = [{
    period: 'weekly',
    name: 'Weekly',
    stripe_interval: 'week',
    stripe_interval_count: 1,
    interval: 1,
  }, {
    period: 'monthly',
    interval: 1,
    stripe_interval: 'month',
    stripe_interval_count: 1,
    name: 'Monthly',
  }, {
    period: 'monthly',
    interval: 3,
    stripe_interval: 'month',
    stripe_interval_count: 3,
    name: 'Quarterly',
  }, {
    period: 'yearly',
    interval: 1,
    stripe_interval: 'year',
    stripe_interval_count: 1,
    name: 'Yearly',
  },
  {
    period: 'one_time',
    interval: 1,
    stripe_interval: 'year',
    stripe_interval_count: 1,
    name: 'One Time',
  }
  ]
  prouductId;
  index;
  featuresArray = [{}];
  selectedProduct = {} as any;
  constructor(
    public router: Router, public productService: ProductService,
    public commonService: CommonService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(event => {
      if (event) {
        this.prouductId = event.id;
        this.index = event.index;
        if (this.prouductId) {
          this.productService.getProductsById(this.prouductId).subscribe((resp: any) => {
            this.selectedProduct = resp;
            if (this.index !== undefined) {
              this.pricingObject = resp.prices[this.index];
              console.log(this.pricingObject);
              if (resp.prices[this.index].featuresArray && resp.prices[this.index].featuresArray.length > 0) {
                this.featuresArray = resp.prices[this.index].featuresArray;
              } else {
                this.featuresArray = [{}];
              }

            }
          });
        }
      }
    });
    this.getProductData();
    this.router.events.subscribe((event: NavigationStart) => {
      if (event instanceof NavigationStart) {
        this.featuresArray = [{}];
      }
    });
  }

  getProductData() {
    this.products = [];
    this.productService.getProducts().subscribe(resp => {
      this.products = [];
      resp.map(data => {
        this.products.push(data);
      });
      this.products.sort((a, b) => parseInt(a.orderby) - parseInt(b.orderby));
    });
  }


  addMoreInput() {
    this.featuresArray.push({});
  }

  trackByFn(index, item) {
    return index;
  }


  removeInput(index) {
    this.featuresArray.splice(index, 1);
  }

  addPrice() {
    const planObject = {} as any;
    this.commonService.presentIndicator();
    planObject.period = this.pricingObject.plan.period;
    planObject.interval = this.pricingObject.plan.interval;
    planObject.price = parseInt(this.pricingObject.price);
    if (this.index === undefined) {
      if (this.pricingObject.currency === 'INR') {
        if (this.pricingObject.plan.period === 'one_time') {
          this.createPrice({ status: true });
        } else {
          this.productService.createPlan(planObject).subscribe(resp => {
            this.commonService.dismissIndicator();
            this.createPrice(resp);
          })
        }

      } else {
        if (this.pricingObject.plan.period === 'one_time') {
          if (this.pricingObject.stripe_price_id) {
            this.productService.createStripeOneTimePrice({ unit_amount: planObject.price * 100, stripe_price_id: this.pricingObject.stripe_price_id, currency: this.pricingObject.currency, ... this.pricingObject.plan, stripe_product_id: this.selectedProduct.stripe_product_id }).subscribe(createStripe => {
              this.createPrice(createStripe, 'method');
            });
          } else {
            this.productService.createStripeOneTimePrice({ unit_amount: planObject.price * 100, currency: this.pricingObject.currency, ... this.pricingObject.plan, stripe_product_id: this.selectedProduct.stripe_product_id }).subscribe(createStripe => {
              this.createPrice(createStripe, 'method');
            });
          }

        } else {
          if (this.pricingObject.stripe_price_id) {
            this.productService.updateStripePrice({ unit_amount: planObject.price * 100, stripe_price_id: this.pricingObject.stripe_price_id, currency: this.pricingObject.currency, ... this.pricingObject.plan, stripe_product_id: this.selectedProduct.stripe_product_id }).subscribe(createStripe => {
              this.createPrice(createStripe, 'method');
            });
          } else {
            this.productService.createStripePrice({ unit_amount: planObject.price * 100, currency: this.pricingObject.currency, ... this.pricingObject.plan, stripe_product_id: this.selectedProduct.stripe_product_id }).subscribe(createStripe => {
              this.createPrice(createStripe, 'method');
            });
          }
        }

      }
    } else {
      this.createPrice({ status: true }, '', true);
    }



  }


  createPrice(resp, method?, isData?) {
    if (resp.status) {
      try {
        if (!isData && resp.data) {
          const parseData = JSON.parse(resp.data);
          if (parseData.id) {
            this.pricingObject.plan_id = parseData.id;
          }
          if (method) {
            this.pricingObject.stripe_price_id = parseData.id;
          }
        }
        if (!this.selectedProduct.prices) {
          this.selectedProduct.prices = [];
          this.selectedProduct.prices.push(this.pricingObject);
        } else {
          if (this.index === undefined) {
            this.selectedProduct.prices.push(this.pricingObject);
          } else {
            this.selectedProduct.prices[this.index] = this.pricingObject;
          }

        }
        if (this.selectedProduct && this.selectedProduct.productDocId) {
          this.productService.updateProduct(this.selectedProduct.productDocId, this.selectedProduct).then(resp => {
            this.commonService.dismissIndicator();
            if (this.prouductId && this.index !== undefined) {
              this.commonService.showAlert('Price updated for selected product');
            } else {
              this.featuresArray = [{}];
              this.pricingObject = {
                featuresArray: [],
                currency: 'INR'
              };
              this.commonService.showAlert('Price added for selected product');
            }
          });
        }
      } catch (e) {
        this.commonService.dismissIndicator();
      }

    } else {
      this.commonService.dismissIndicator();
    }
  }


  compareWith(item, item2) {
    console.log(item)
    return item.name === item2.name;
  }

}
