import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddPricePageRoutingModule } from './add-price-routing.module';

import { AddPricePage } from './add-price.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    AddPricePageRoutingModule
  ],
  declarations: [AddPricePage]
})
export class AddPricePageModule { }
