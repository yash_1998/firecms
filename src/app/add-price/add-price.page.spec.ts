import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddPricePage } from './add-price.page';

describe('AddPricePage', () => {
  let component: AddPricePage;
  let fixture: ComponentFixture<AddPricePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddPricePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddPricePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
