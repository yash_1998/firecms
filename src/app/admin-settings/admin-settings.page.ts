import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, NavigationStart } from '@angular/router';
import { ProductService } from '../service/product.service';
import { CommonService } from '../common.service';
import { Storage } from '@ionic/storage';
import * as firebase from 'firebase/app';
import { Plugins } from '@capacitor/core';
import { CrudService } from '../service/crud.service';
import { PolicyService } from '../service/policy.service';


@Component({
  selector: 'app-admin-settings',
  templateUrl: './admin-settings.page.html',
  styleUrls: ['./admin-settings.page.scss'],
})
export class AdminSettingsPage implements OnInit {
  timeinData = {} as any;
  prouductId;
  index;
  featuresArray = [{}];
  users = [];
  userData = {} as any;
  restrictData = {} as any;
  selectedProduct = {} as any;
  constructor(
    public policyService: PolicyService,
    private storage: Storage, public crudService: CrudService,
    public router: Router, public productService: ProductService,
    public commonService: CommonService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(event => {
      if (event) {
        this.prouductId = event.id;
        this.index = event.index;
        if (this.prouductId) {
          this.productService.getProductsById(this.prouductId).subscribe((resp: any) => {
            this.selectedProduct = resp;
          });
        }
      }
    });

    this.getUser();
    this.router.events.subscribe((event: NavigationStart) => {
      if (event instanceof NavigationStart) {
        this.getUser();
      }
    });
  }

  getUser() {
    this.storage.get('loggedInUser').then((val) => {
      if (val) {
        this.productService.getProductsById(this.prouductId).subscribe((resp: any) => {
          this.productService.getProductUser(this.prouductId, val).then(snap => {
            if (snap.data()) {
              this.restrictData = snap.data();
            }
          });
        })
        this.crudService.getUserData(val).then(userData => {
          console.log(userData);
          userData.forEach(doc => {
            this.userData = doc.data();
            console.log(doc.data());
          })
          this.crudService.getNewStudentsSettings(this.userData.authId, this.prouductId).subscribe(data => {
            if (data) {
              this.timeinData = data;
            }

          })
          this.crudService.getUsrByCompanyName(this.userData.company_code).then((res: any) => {
            if (res) {
              this.users = [];
              res.forEach(doc => {
                this.users.push(doc.data())
                console.log(doc.data());
              })
            }
            console.log(this.users);
          })
        })
      }
    })
  }


  saveData() {
    this.crudService.showLoader();
    this.timeinData.key = this.selectedProduct.key;
    this.crudService.setNewStudentsSettings(this.userData.authId, this.prouductId, this.timeinData).then(resp => {
      this.crudService.showToast('Data saved');
      this.crudService.hideLoader();
      this.createGroup(this.timeinData);
    })
  }

  createGroup(data) {
    this.policyService.createGroup(data.group_url + data.group_id, data).subscribe(resp => {
      this.productService.train(data.group_url + data.group_id + '/train', {}, data).subscribe(resp => {

      })
    })
  }


}
