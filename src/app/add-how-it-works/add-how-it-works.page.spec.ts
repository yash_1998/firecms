import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddHowItWorksPage } from './add-how-it-works.page';

describe('AddHowItWorksPage', () => {
  let component: AddHowItWorksPage;
  let fixture: ComponentFixture<AddHowItWorksPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddHowItWorksPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddHowItWorksPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
