import { Component, OnInit } from '@angular/core';
import { PolicyService } from '../service/policy.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductService } from '../service/product.service';
import { CommonService } from '../common.service';
import { UploadService } from '../service/upload.service';

@Component({
  selector: 'app-add-how-it-works',
  templateUrl: './add-how-it-works.page.html',
  styleUrls: ['./add-how-it-works.page.scss'],
})
export class AddHowItWorksPage implements OnInit {
  howitWorkObject = {} as any;
  fileObject;
  prouductId: any;
  index: any;
  selectedProduct = {} as any;
  constructor(public router: Router, public productService: ProductService,
    public uploadService: UploadService,
    public commonService: CommonService, private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.params.subscribe(event => {
      if (event) {
        this.prouductId = event.id;
        this.index = event.index;
        if (this.prouductId) {
          this.productService.getProductsById(this.prouductId).subscribe((resp: any) => {
            this.selectedProduct = resp;
            if (this.index !== undefined) {
              this.howitWorkObject = resp.howitworks[this.index];
            }
          });
        }
      }
    });
  }

  imageUpload(event) {
    if (event.target.files[0]) {
      const file: File = event.target.files[0];
      this.fileObject = event.target.files[0];
      if (!this.selectedProduct.howitworks) {
        this.selectedProduct.howitworks = [];
      }
      this.uploadService.ImageUpload(this.fileObject, ('Images/' + new Date().getTime() + '/')).then(downLoadUrl => {
        this.howitWorkObject.url = downLoadUrl;
      });
    } else {
      // this.alertService.dismissLoader();
    }
  }

  addPrice() {
    if (!this.selectedProduct.howitworks) {
      this.selectedProduct.howitworks = [];
      this.selectedProduct.howitworks.push(this.howitWorkObject);
    } else {
      if (this.index === undefined) {
        this.selectedProduct.howitworks.push(this.howitWorkObject);
      } else {
        this.selectedProduct.howitworks[this.index] = this.howitWorkObject;
      }
    }
    console.log(this.selectedProduct);
    if (this.selectedProduct && this.selectedProduct.productDocId) {
      this.productService.updateProduct(this.selectedProduct.productDocId, this.selectedProduct).then(resp => {
        if (this.prouductId && this.index !== undefined) {
          this.commonService.showAlert('How it works updated for selected product');
        } else {
          this.howitWorkObject = {};
          // this.featuresArray = [{}];
          this.commonService.showAlert('How it works added for selected product');
        }
      });
    }
  }

}
