import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddHowItWorksPageRoutingModule } from './add-how-it-works-routing.module';

import { AddHowItWorksPage } from './add-how-it-works.page';
import { ComponentsModule } from '../components/components.module';
import { AngularEditorModule } from '@kolkov/angular-editor';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    AngularEditorModule,
    AddHowItWorksPageRoutingModule
  ],
  declarations: [AddHowItWorksPage]
})
export class AddHowItWorksPageModule { }
