import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddHowItWorksPage } from './add-how-it-works.page';

const routes: Routes = [
  {
    path: '',
    component: AddHowItWorksPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddHowItWorksPageRoutingModule {}
