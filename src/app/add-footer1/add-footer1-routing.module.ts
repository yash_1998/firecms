import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddFooter1Page } from './add-footer1.page';

const routes: Routes = [
  {
    path: '',
    component: AddFooter1Page
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddFooter1PageRoutingModule {}
