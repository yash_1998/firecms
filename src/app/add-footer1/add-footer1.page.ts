import { Component, OnInit } from '@angular/core';
import { PolicyService } from '../service/policy.service';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-add-footer1',
  templateUrl: './add-footer1.page.html',
  styleUrls: ['./add-footer1.page.scss'],
})
export class AddFooter1Page implements OnInit {
  footer1Object = {
    id: 'footer1'
  } as any;
  constructor(public policyService: PolicyService, public commonService: CommonService) { }

  ngOnInit() {
    this.policyService.getExtraInfoId('footer1').subscribe(resp => {
      this.footer1Object = resp;
    });
  }


  setFooter1() {
    this.policyService.setExtraInfo('footer1', this.footer1Object).then(resp => {
      this.commonService.showAlert('Footer 1 updated');
    });
  }



}
