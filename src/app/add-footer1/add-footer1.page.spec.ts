import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddFooter1Page } from './add-footer1.page';

describe('AddFooter1Page', () => {
  let component: AddFooter1Page;
  let fixture: ComponentFixture<AddFooter1Page>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddFooter1Page ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddFooter1Page);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
