import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddFooter1PageRoutingModule } from './add-footer1-routing.module';

import { AddFooter1Page } from './add-footer1.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    AddFooter1PageRoutingModule
  ],
  declarations: [AddFooter1Page]
})
export class AddFooter1PageModule { }
