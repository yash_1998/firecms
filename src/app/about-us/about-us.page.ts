import { Component, OnInit } from '@angular/core';
import { UploadService } from '../service/upload.service';
import { PolicyService } from '../service/policy.service';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-about-us',
  templateUrl: './about-us.page.html',
  styleUrls: ['./about-us.page.scss'],
})
export class AboutUsPage implements OnInit {
  aboutusObject = {
    id: 'aboutus'
  } as any;
  constructor(public uploadService: UploadService, public policyService: PolicyService, public commonService: CommonService) { }

  ngOnInit() {
    this.policyService.getExtraInfoId('aboutus').subscribe(resp => {
      if (resp) {
        this.aboutusObject = resp;
        console.log(this.aboutusObject);
      }
    })
  }


  imageUpload(event) {
    this.uploadService.ImageUpload(event.target.files[0], ('Images/aboutus/')).then(downLoadUrl => {
      this.aboutusObject.url = downLoadUrl;
    });
  }

  setAboutUs() {
    this.policyService.setExtraInfo('aboutus', this.aboutusObject).then(resp => {
      this.commonService.showAlert('About Us updated');
    });
  }

}
