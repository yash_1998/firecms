import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LoginWvPage } from './login-wv.page';

const routes: Routes = [
  {
    path: '',
    component: LoginWvPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoginWvPageRoutingModule {}
