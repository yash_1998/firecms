import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LoginWvPageRoutingModule } from './login-wv-routing.module';

import { LoginWvPage } from './login-wv.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ReactiveFormsModule,
    LoginWvPageRoutingModule
  ],
  declarations: [LoginWvPage]
})
export class LoginWvPageModule {}
