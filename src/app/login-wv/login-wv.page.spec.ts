import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { LoginWvPage } from './login-wv.page';

describe('LoginWvPage', () => {
  let component: LoginWvPage;
  let fixture: ComponentFixture<LoginWvPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginWvPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(LoginWvPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
