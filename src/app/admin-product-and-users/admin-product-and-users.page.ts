import { Component, OnInit } from '@angular/core';
import { NavigationStart, Router, ActivatedRoute } from '@angular/router';
import { FeatureService } from '../service/feature.service';
import { CommonService } from '../common.service';
import { Plugins } from '@capacitor/core';
const { Browser } = Plugins;
import * as moment from 'moment';
import { Storage } from '@ionic/storage';

import { ProductService } from '../service/product.service';
import { PolicyService } from '../service/policy.service';
import { CrudService } from '../service/crud.service';


@Component({
  selector: 'app-admin-product-and-users',
  templateUrl: './admin-product-and-users.page.html',
  styleUrls: ['./admin-product-and-users.page.scss'],
})
export class AdminProductAndUsersPage implements OnInit {
  adminUserId = '';
  adminUserData = {} as any;
  normalUser = [];
  activeProducts = [];
  constructor(
    public crudService: CrudService,
    public router: Router, private storage: Storage, public route: ActivatedRoute, public featureService: FeatureService,
    public commonService: CommonService, public productService: ProductService, public policyService: PolicyService) {
  }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log(params)
      this.adminUserId = this.route.snapshot.params.id;
      console.log(this.adminUserId, 'this.adminUserId');
      this.normalUsers();
      this.loadActiveProducts();
      this.crudService.getUsersById(this.adminUserId).subscribe(adminData => {
      });
    })
  }


  normalUsers() {
    this.crudService.getNormalUsersByAdminId(this.adminUserId).then(snapshot => {
      this.normalUser = [];
      snapshot.forEach(doc => {
        this.normalUser.push(doc.data());
      });
    })
  }



  loadActiveProducts() {
    this.crudService.getActiveProductByAdminId(this.adminUserId).then(snapshot => {
      this.activeProducts = [];
      snapshot.forEach(doc => {
        this.activeProducts.push(doc.data());
      });
      console.log(this.activeProducts, 'test')
    })
  }

  getFormatedDate(item) {
    if (item && item.end_date) {
      return moment(item.end_date).format('DD/MM/YYYY');
    }
  }


  goToProductDetails(item) {
    this.router.navigate(['admin-product-details', this.adminUserId, item.productDocId]);
  }

}
