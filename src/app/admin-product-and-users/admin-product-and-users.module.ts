import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AdminProductAndUsersPageRoutingModule } from './admin-product-and-users-routing.module';

import { AdminProductAndUsersPage } from './admin-product-and-users.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    AdminProductAndUsersPageRoutingModule
  ],
  declarations: [AdminProductAndUsersPage]
})
export class AdminProductAndUsersPageModule { }
