import { Component, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-whatsagent',
  templateUrl: './whatsagent.page.html',
  styleUrls: ['./whatsagent.page.scss'],
})
export class WhatsagentPage implements OnInit {
  shwarr=[];text;arraylen=0;
  disablemsgbtn;
  @ViewChild('divfocus',{static:false}) divfocus:any;
  constructor() { }

  ngOnInit() {
  }
  addData()
  {
    console.log(this.text);
    this.shwarr.push({
      'MSG':this.text,
      'IMG':'N'
    })
    this.arraylen=this.shwarr.length;
    this.text='';
    
    this.divfocus.scrollToBottom(0);
    // for(let i=0;i<this.shwarr.length;i++){
    //   if(i==this.shwarr.length-1)
    //   {
    //     setTimeout(() =>this.divfocus.setFocus(), 300);
    //   }
    // }
    
  }
  addImg()
  {
    console.log(this.shwarr);
    this.shwarr.push({
      'MSG':'assets/img/user.png',
      'IMG':'Y'
    })
    this.arraylen=this.shwarr.length;
    
    this.divfocus.scrollToBottom(0);
  }
}
