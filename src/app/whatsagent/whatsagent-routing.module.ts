import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WhatsagentPage } from './whatsagent.page';

const routes: Routes = [
  {
    path: '',
    component: WhatsagentPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WhatsagentPageRoutingModule {}
