import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { WhatsagentPage } from './whatsagent.page';

describe('WhatsagentPage', () => {
  let component: WhatsagentPage;
  let fixture: ComponentFixture<WhatsagentPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatsagentPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(WhatsagentPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
