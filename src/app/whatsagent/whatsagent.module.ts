import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { WhatsagentPageRoutingModule } from './whatsagent-routing.module';

import { WhatsagentPage } from './whatsagent.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    WhatsagentPageRoutingModule
  ],
  declarations: [WhatsagentPage]
})
export class WhatsagentPageModule {}
