import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthenticateService } from '../service/authentication.service';
import { UserService } from '../user.service';
import { Location } from '@angular/common';
import { CrudService } from '../service/crud.service';
import { Router, ActivatedRoute } from '@angular/router';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-free-user-creation',
  templateUrl: './free-user-creation.page.html',
  styleUrls: ['./free-user-creation.page.scss'],
})
export class FreeUserCreationPage implements OnInit {
  loginObject = {} as any;
  errorMessage: string = '';
  successMessage: string = '';
  timezone;
  userId: string;
  timezoneName = [];
  userEmail: string;
  id = '';
  usrName: string; usrLastName: string; phno: string;
  validations_form: FormGroup; validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };

  constructor(public route: ActivatedRoute, public location: Location, public commonService: CommonService, public router: Router, private formBuilder: FormBuilder, private authService: AuthenticateService, public user: UserService, private crudService: CrudService) { }

  ngOnInit() {
    this.route.queryParams.subscribe(params => {
      console.log(params)
      this.id = this.route.snapshot.params.id;
    });
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
  }

  tryRegister(value) {
    this.authService.registerUser(value)
      .then(res => {
        this.loginObject.password = value.password;
        console.log(res);
        this.errorMessage = "";
        this.successMessage = "Your account has been created. Please verify your email and log in.";
        const currentUser = this.authService.userDetails();
        currentUser.sendEmailVerification({
          url: 'https://officeplus-ab43b.firebaseapp.com/login'
        });
        this.commonService.showToast('Verification link sent to your email address');
        this.userId = this.authService.userDetails().uid;
        this.userEmail = this.authService.userDetails().email;
        console.log(this.userId);
        console.log(this.userEmail);
        console.log(this.usrName);
        this.CreateRecord(value);
      }, err => {
        console.log(err);
        this.errorMessage = err.message;
        this.successMessage = "";
      })
  }

  CreateRecord(data) {
    let record = {};
    this.user.registered_username = this.usrName;
    this.loginObject.email = data.email;
    this.loginObject.role = "admin";
    this.loginObject.authId = this.userId;
    this.loginObject.adminUserId = this.userId;
    this.loginObject.company_name = this.loginObject.company_name.trim();
    this.loginObject.company_code = this.loginObject.company_name + new Date().getTime();
    this.crudService.setNewStudents(this.userId, this.loginObject).then(resp => {
      if (this.id) {
        this.location.back();
      } else {
        this.goToLogin()
      }

      //   this.studentName = "";
      //   this.studentAge = undefined;
      //   this.studentAddress = "";
      //   this.authId="";
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }



  goToLogin() {
    this.router.navigate(['login']);
  }

}
