import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FreeUserCreationPage } from './free-user-creation.page';

describe('FreeUserCreationPage', () => {
  let component: FreeUserCreationPage;
  let fixture: ComponentFixture<FreeUserCreationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeUserCreationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FreeUserCreationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
