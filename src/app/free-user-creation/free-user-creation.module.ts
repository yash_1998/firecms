import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FreeUserCreationPageRoutingModule } from './free-user-creation-routing.module';

import { FreeUserCreationPage } from './free-user-creation.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FormsModule,
    IonicModule,
    FreeUserCreationPageRoutingModule
  ],
  declarations: [FreeUserCreationPage]
})
export class FreeUserCreationPageModule { }
