import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FreeUserCreationPage } from './free-user-creation.page';

const routes: Routes = [
  {
    path: '',
    component: FreeUserCreationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FreeUserCreationPageRoutingModule {}
