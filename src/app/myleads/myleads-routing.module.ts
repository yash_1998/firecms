import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyleadsPage } from './myleads.page';

const routes: Routes = [
  {
    path: '',
    component: MyleadsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyleadsPageRoutingModule {}
