import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyleadsPage } from './myleads.page';

describe('MyleadsPage', () => {
  let component: MyleadsPage;
  let fixture: ComponentFixture<MyleadsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyleadsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyleadsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
