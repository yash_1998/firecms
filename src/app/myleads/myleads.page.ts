import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Plugins } from '@capacitor/core';
import { Storage } from '@ionic/storage';
import { CrudService } from '../service/crud.service';

const { Network } = Plugins;

@Component({
  selector: 'app-myleads',
  templateUrl: './myleads.page.html',
  styleUrls: ['./myleads.page.scss'],
})
export class MyleadsPage implements OnInit {
  userData = {} as any;
  leads = [];
  constructor(public router: Router, public crudService: CrudService, private storage: Storage, public productService: ProductService, ) { }

  ngOnInit() {
    this.storage.get('loggedInUser').then((val) => {
      if (val) {
        this.crudService.getUserData(val).then(userData => {
          console.log(userData);
          userData.forEach(doc => {
            this.userData = doc.data();
            console.log(doc.data());
          })
        })
      }
      console.log('L-Your user ID is', val);
      if (val != null || "") {
        console.log("L-some one is logged in");
        //  this.navCtrl.navigateRoot('/dashboard');
      } else {
        console.log("L-no one is logged in");
      }
    });
    this.productService.getLeads().subscribe(async resp => {
      let status = await Network.getStatus();
      this.leads = [];
      resp.forEach((actions) => {
        const body: any = actions.payload.doc.data();
        if (!body.leadDocId || (body.offline && status.connected)) {
          if (status.connected) {
            body.offline = false;
          }
          body.leadDocId = actions.payload.doc.id;
          this.leads.push(body);
          this.productService.updateLeads(actions.payload.doc.id, body).then(resp => {
          });
        } else {
          // if (status.connected) {
          //   body.offline = false;
          //   this.productService.updateLeads(actions.payload.doc.id, body).then(resp => {
          //   });
          // }
          this.leads.push(body);
        }

      })
      console.log(this.leads);
      // this.leads = resp;
      // this.leads.sort((a, b) => parseInt(a.orderby) - parseInt(b.orderby));
    });
  }

  editLead(leadDocId) {
    this.router.navigate(['edit-lead', leadDocId]);
  }

  goToLeadChat(leadDocId) {
    this.router.navigate(['mylead-chat', leadDocId]);
  }

  deleteLead(id) {
    this.productService.deleteLead(id).then(resp => {

    })
  }

}
