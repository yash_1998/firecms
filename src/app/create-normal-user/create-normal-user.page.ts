import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';
import { AuthenticateService } from '../service/authentication.service';
import { UserService } from '../user.service';
import { CrudService } from '../service/crud.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import * as firebase from 'firebase/app';
import { Storage } from '@ionic/storage';
import { CommonService } from '../common.service';

@Component({
  selector: 'app-create-normal-user',
  templateUrl: './create-normal-user.page.html',
  styleUrls: ['./create-normal-user.page.scss'],
})
export class CreateNormalUserPage implements OnInit {
  loginObject = {} as any;
  errorMessage: string = '';
  userData = {} as any;
  successMessage: string = '';
  timezone;
  normalId = '';
  userId: string;
  userTypes = ['normal', 'admin'];
  timezoneName = [];
  userEmail: string;
  usrName: string; usrLastName: string; phno: string;
  validations_form: FormGroup; validation_messages = {
    'email': [
      { type: 'required', message: 'Email is required.' },
      { type: 'pattern', message: 'Enter a valid email.' }
    ],
    'password': [
      { type: 'required', message: 'Password is required.' },
      { type: 'minlength', message: 'Password must be at least 5 characters long.' }
    ]
  };

  constructor(public commonService: CommonService, private route: ActivatedRoute, private storage: Storage, public location: Location, public router: Router, private formBuilder: FormBuilder, private authService: AuthenticateService, public user: UserService, private crudService: CrudService) { }

  ngOnInit() {
    this.route.params.subscribe(event => {
      if (event) {
        this.normalId = event.id;
        if (this.normalId) {
          this.crudService.getUserData(this.normalId).then(userData => {
            userData.forEach(doc => {
              this.loginObject = doc.data();
              this.validations_form.get('email').setValue(this.loginObject.email);
              this.validations_form.get('password').setValue(this.loginObject.password);
            })

          })
        }
      }
    });
    this.validations_form = this.formBuilder.group({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')
      ])),
      password: new FormControl('', Validators.compose([
        Validators.minLength(5),
        Validators.required
      ])),
    });
    this.storage.get('loggedInUser').then((val) => {
      if (val) {
        this.crudService.getUserData(val).then(userData => {
          console.log(userData);
          userData.forEach(doc => {
            this.userData = doc.data();
            console.log(doc.data());
          })
        })
      }
    })
  }

  tryRegister(value) {
    this.authService.registerUser(value)
      .then(res => {
        this.loginObject.password = value.password;
        console.log(res);
        this.errorMessage = "";
        this.successMessage = "Your account has been created. Please verify your email and log in.";
        const currentUser = this.authService.userDetails();
        currentUser.sendEmailVerification({
          url: 'https://officeplus-ab43b.firebaseapp.com/login'
        });
        this.commonService.showToast('Verification link sent to your email address ');
        this.userId = this.authService.userDetails().uid;
        this.userEmail = this.authService.userDetails().email;
        console.log(this.userId);
        console.log(this.userEmail);
        console.log(this.usrName);
        this.CreateRecord(value);
      }, err => {
        console.log(err);
        this.errorMessage = err.message;
        this.successMessage = "";
      })
  }



  signInMethod(type) {
    firebase.auth().signInWithEmailAndPassword(this.loginObject.email, this.loginObject.password).then(resp => {
      console.log(resp, resp.user);
      if (resp) {
        if (type === 'email') {
          resp.user.updateEmail(this.validations_form.get('email').value).then(resp => {
            this.loginObject.email = this.validations_form.get('email').value;
            this.updateData();
          });
        }
        if (type === 'password') {
          resp.user.updatePassword(this.validations_form.get('password').value).then(resp => {
            this.loginObject.password = this.validations_form.get('password').value;
            this.updateData();
          });
        }

      }
    })
  }


  updateUserData() {
    if (this.validations_form.get('email').value !== this.loginObject.email) {
      console.log(this.validations_form.get('email').value)
      this.signInMethod('email');
    }
    if (this.validations_form.get('password').value !== this.loginObject.password) {
      console.log(this.validations_form.get('password').value)
      this.signInMethod('password');
    }
  }


  updateData() {
    this.crudService.updateNewStudents(this.loginObject.authId, this.loginObject).then(resp => {
      this.location.back();
      firebase.auth().signInWithEmailAndPassword(this.userData.email, this.userData.password);
    })
  }

  CreateRecord(data) {
    let record = {};
    this.user.registered_username = this.usrName;
    this.loginObject.email = data.email;
    this.loginObject.authId = this.userId;
    this.loginObject.company_name = this.userData.company_name;
    this.loginObject.company_code = this.userData.company_code;
    this.loginObject.adminUserId = this.userData.authId;
    this.crudService.setNewStudents(this.userId, this.loginObject).then(resp => {
      this.location.back();
      //   this.studentName = "";
      //   this.studentAge = undefined;
      //   this.studentAddress = "";
      //   this.authId="";
      console.log(resp);
    })
      .catch(error => {
        console.log(error);
      });
  }



  goToLogin() {
    this.router.navigate(['login']);
  }
}
