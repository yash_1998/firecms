import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CreateNormalUserPageRoutingModule } from './create-normal-user-routing.module';

import { CreateNormalUserPage } from './create-normal-user.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ReactiveFormsModule,
    FormsModule,
    CreateNormalUserPageRoutingModule
  ],
  declarations: [CreateNormalUserPage]
})
export class CreateNormalUserPageModule { }
