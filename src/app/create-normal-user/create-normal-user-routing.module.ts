import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CreateNormalUserPage } from './create-normal-user.page';

const routes: Routes = [
  {
    path: '',
    component: CreateNormalUserPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CreateNormalUserPageRoutingModule {}
