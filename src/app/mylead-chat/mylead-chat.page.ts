import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { Router, ActivatedRoute } from '@angular/router';
import * as firebase from 'firebase/app';
import * as moment from 'moment';
import { UploadService } from '../service/upload.service';
import { CommonService } from '../common.service';

import { Plugins } from '@capacitor/core';


const { Network } = Plugins;

@Component({
  selector: 'app-mylead-chat',
  templateUrl: './mylead-chat.page.html',
  styleUrls: ['./mylead-chat.page.scss'],
})
export class MyleadChatPage implements OnInit {
  public leadId = '';
  public leadObject = {} as any;
  public remark = '';
  constructor(
    public productService: ProductService,
    public commonService: CommonService,
    private route: ActivatedRoute,
    public router: Router, public uploadService: UploadService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(event => {
      if (event && event.id) {
        this.leadId = event.id;
        this.productService.getLeadById(this.leadId).subscribe(resp => {
          this.leadObject = resp;
          console.log(this.leadObject);
          if (this.leadObject.remarks) {
            this.leadObject.remarks.sort((a, b) => parseInt(b.date) - parseInt(a.date));
            this.leadObject.remarks.forEach(async (element, index) => {
              let status = await Network.getStatus();
              if (element.offline && status.connected) {
                this.leadObject.remarks[index].offline = false;
                console.log(this.leadObject, 'test');
                this.productService.updateLeads(this.leadId, this.leadObject).then(resp => {
                });
              }
            });
          }
          console.log(resp);
        });
      }
      console.log(event);
    });
  }


  async addRemarks() {
    // 1st time when there is no array
    let status = await Network.getStatus();
    if (!this.leadObject.remarks) {
      this.leadObject.remarks = [];
      this.leadObject.remarks.push({ offline: true, remark: this.remark, date: new Date().getTime() });
      // from 2nd time else will work
    } else {
      this.leadObject.remarks.push({ offline: true, remark: this.remark, date: new Date().getTime() });
    }
    firebase.firestore().disableNetwork()
      .then(() => {
        this.commonService.showAlert('Remark saved');
        // Do offline actions
        // ...
      });
    this.productService.updateLeads(this.leadId, this.leadObject).then(resp => {
      this.commonService.showAlert('Remark saved');
    })
  }

  getDateAndTime(date) {
    return moment(date).format('DD/MMMM/YYYY HH:mm');
  }

}
