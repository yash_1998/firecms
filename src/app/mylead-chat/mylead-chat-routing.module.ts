import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MyleadChatPage } from './mylead-chat.page';

const routes: Routes = [
  {
    path: '',
    component: MyleadChatPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class MyleadChatPageRoutingModule {}
