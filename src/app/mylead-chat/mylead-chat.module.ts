import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { MyleadChatPageRoutingModule } from './mylead-chat-routing.module';

import { MyleadChatPage } from './mylead-chat.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    MyleadChatPageRoutingModule
  ],
  declarations: [MyleadChatPage]
})
export class MyleadChatPageModule {}
