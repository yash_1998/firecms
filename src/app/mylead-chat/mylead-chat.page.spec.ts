import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { MyleadChatPage } from './mylead-chat.page';

describe('MyleadChatPage', () => {
  let component: MyleadChatPage;
  let fixture: ComponentFixture<MyleadChatPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MyleadChatPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(MyleadChatPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
