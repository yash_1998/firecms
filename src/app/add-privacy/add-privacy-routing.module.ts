import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddPrivacyPage } from './add-privacy.page';

const routes: Routes = [
  {
    path: '',
    component: AddPrivacyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AddPrivacyPageRoutingModule {}
