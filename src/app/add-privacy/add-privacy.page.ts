import { Component, OnInit, OnDestroy } from '@angular/core';
import { Editor } from 'ngx-editor';
import { PolicyService } from '../service/policy.service';
import { CommonService } from '../common.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-add-privacy',
  templateUrl: './add-privacy.page.html',
  styleUrls: ['./add-privacy.page.scss'],
})
export class AddPrivacyPage implements OnInit, OnDestroy {

  policyObject = {} as any;
  polices = [];
  policyType = [{
    name: 'Privacy',
    value: 'privacy'
  }, {
    name: 'Terms & conditions',
    value: 'terms_and_conditions'
  }, {
    name: 'Return & Refund',
    value: 'return_and_refund'
  }];
  constructor(public policyService: PolicyService, public commonService: CommonService, private route: ActivatedRoute) { }

  editor: Editor;
  html: '';

  ngOnInit(): void {
    this.route.params.subscribe(event => {
      console.log(event);
    });
    this.editor = new Editor();
    this.policyService.getExtraInfo().subscribe((resp) => {
      this.polices = [];
      resp.forEach((item: any) => {
        if (item.policy) {
          this.polices.push(item);
        }
        console.log(this.polices);
      });
    });
  }

  // make sure to destory the editor
  ngOnDestroy(): void {
    this.editor.destroy();
  }

  addPolicy() {
    this.policyService.setExtraInfo(this.policyObject.policy_type,
      { id: this.policyObject.policy_type, ...this.policyObject, policy: true }).then(resp => {
        this.commonService.showAlert('Policy added');
      });
  }

  editPolicy(item) {
    this.policyObject = item;
  }


  clearPolicy() {
    this.policyObject = {};
  }

  deletePolicy(item) {
    this.policyService.deleteExtraInfo(item.id).then(resp => {

    });
  }
}
