import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxEditorModule } from 'ngx-editor';
import { IonicModule } from '@ionic/angular';

import { AddPrivacyPageRoutingModule } from './add-privacy-routing.module';
import { AngularEditorModule } from '@kolkov/angular-editor';
import { AddPrivacyPage } from './add-privacy.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    AngularEditorModule,
    NgxEditorModule,
    AddPrivacyPageRoutingModule
  ],
  declarations: [AddPrivacyPage]
})
export class AddPrivacyPageModule { }
