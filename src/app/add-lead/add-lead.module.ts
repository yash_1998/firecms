import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AddLeadPageRoutingModule } from './add-lead-routing.module';

import { AddLeadPage } from './add-lead.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    AddLeadPageRoutingModule
  ],
  declarations: [AddLeadPage]
})
export class AddLeadPageModule {}
