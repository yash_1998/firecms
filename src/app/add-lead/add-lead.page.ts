import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { ProductService } from '../service/product.service';
import { Plugins } from '@capacitor/core';
import { Router, ActivatedRoute } from '@angular/router';
import { UploadService } from '../service/upload.service';
import { CommonService } from '../common.service';
import { Storage } from '@ionic/storage';
import * as moment from 'moment';
import * as firebase from 'firebase/app';
import { IonInput } from '@ionic/angular';
import 'moment-timezone';
import { CrudService } from '../service/crud.service';
const { Network } = Plugins;
@Component({
  selector: 'app-add-lead',
  templateUrl: './add-lead.page.html',
  styleUrls: ['./add-lead.page.scss'],
})
export class AddLeadPage implements OnInit {
  @ViewChild('inputId', { static: false }) inputEl: IonInput;
  leadObject = {
    status: 'open'
  } as any; leadId;
  options = ['open', 'converted', 'closed'];
  userId = '';
  userData = {} as any;
  constructor(
    public productService: ProductService,
    public commonService: CommonService,
    public crudService: CrudService,
    private route: ActivatedRoute,
    private storage: Storage,
    private changeDetectorRef: ChangeDetectorRef,
    public router: Router, public uploadService: UploadService
  ) { }

  ngOnInit() {
    this.storage.get('loggedInUser').then((val) => {
      if (val) {
        this.userId = val;
      }
      if (val) {
        this.crudService.getUserData(val).then(userData => {
          console.log(userData);
          userData.forEach(doc => {
            this.userData = doc.data();
            console.log(doc.data());
          })
          this.changeDetectorRef.detectChanges();
        })
      }
      console.log('L-Your user ID is', val);
      if (val != null || "") {
        console.log("L-some one is logged in");
        //  this.navCtrl.navigateRoot('/dashboard');
      } else {
        console.log("L-no one is logged in");
      }
    });
    //[iss page pe kisi aur page se aayen to ye chalega] jab myleads se aayega tab iska use hoga, jo docid aayegi, uska data leke aayega
    this.route.params.subscribe(event => {
      if (event && event.id) {
        this.leadId = event.id;
        this.productService.getLeadById(this.leadId).subscribe(resp => {
          this.leadObject = resp;
          console.log(resp);
        });
      }
      console.log(event);
    });
  }

  async addLead() {
    this.leadObject.status = 'open';
    this.leadObject.userId = this.userId;
    console.log(this.userData)
    const body = {} as any;
    body.gmt_time = moment(this.leadObject.actual_time).tz("GMT").valueOf();
    body.remark = this.leadObject.lead_remarks;
    body.actual_time = this.leadObject.actual_time;
    if (this.userData.telegram_id) {
      body.telegram_id = this.userData.telegram_id;
    }
    if (this.userData.company_code) {
      body.company_code = this.userData.company_code;
      this.leadObject.company_code = this.userData.company_code;
    }
    this.crudService.addReminder(body).then(resp => {

    })
    firebase.firestore().disableNetwork()
      .then(() => {
        this.commonService.showAlert('New Lead Added');
        this.leadObject = {
          status: 'open'
        };
        this.inputEl.setFocus();
        // Do offline actions
        // ...
      });
    let status = await Network.getStatus();
    if (!status.connected) {
      this.leadObject.offline = true;
    }
    this.productService.addlead(this.leadObject).then(resp => {
      // this.uploadService.ImageUpload(this.fileObject, ('Images/' + resp.id + '/')).then(downLoadUrl => {
      this.commonService.showAlert('New Lead Added');
      this.leadObject = {
        status: 'open'
      };
      this.inputEl.setFocus();
      this.productService.updateLeads(resp.id, { ...this.leadObject, leadDocId: resp.id }).then(resp => {

      });
      // this.productService.updateProduct(resp.id, { ...this.leadObject, leadDocId: resp.id }).
      //   then(update => {

      //   });
      // });
    }).catch(error => {
      debugger
      console.log(error);
    });
  }

  updateLead() {
    firebase.firestore().disableNetwork()
      .then(() => {
        this.commonService.showAlert('Lead updated');
        // Do offline actions
        // ...
      });
    const body = {} as any;

    body.gmt_time = moment(this.leadObject.actual_time).tz("GMT").valueOf();
    body.remark = this.leadObject.lead_remarks;
    body.actual_time = this.leadObject.actual_time;
    if (this.userData.telegram_id) {
      body.telegram_id = this.userData.telegram_id;
    }
    if (this.userData.company_code) {
      body.company_code = this.userData.company_code;
      this.leadObject.company_code = this.userData.company_code;
    }
    this.crudService.addReminder(body).then(resp => {

    })
    this.productService.updateLeads(this.leadId, this.leadObject).then(resp => {
      this.commonService.showAlert('Lead updated');
    })
  }



}
