import { NgModule } from '@angular/core';
import { BrowserModule, Title } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { ColorSketchModule } from 'ngx-color/sketch';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { Contacts } from '@ionic-native/contacts';
import { CallLog, CallLogObject } from '@ionic-native/call-log/ngx';

import { NgxEditorModule } from 'ngx-editor';
import { AngularEditorModule } from '@kolkov/angular-editor';
import * as firebase from 'firebase';
import { firestore } from 'firebase/app';

//  firebase imports, remove what you don't require
import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule } from '@angular/fire/database';
import { AngularFireStorageModule } from '@angular/fire/storage';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { AngularFireMessagingModule } from '@angular/fire/messaging';

//auth login
import { AuthenticateService } from './service/authentication.service';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { IonicStorageModule } from '@ionic/storage';

//geolocation
import { Geolocation } from '@ionic-native/geolocation/ngx';

// environment
import { environment } from '../environments/environment';

//for scanner
import { BarcodeScanner } from '@ionic-native/barcode-scanner/ngx';
import { Pedometer } from '@ionic-native/pedometer/ngx';

//for sms
import { HttpClient, HttpResponse, HttpHeaders, HttpRequest, HttpClientModule } from '@angular/common/http';
import { HttpModule, Http } from '@angular/http';

//camera
import { Camera } from '@ionic-native/camera/ngx';
import { WebcamModule } from 'ngx-webcam';
import { ServiceWorkerModule } from '@angular/service-worker';

//PWA-ELEMENTS
import { defineCustomElements } from '@ionic/pwa-elements/loader';

import { Diagnostic } from '@ionic-native/diagnostic/ngx';
import { ProductModalComponent } from './product-modal/product-modal.component';
import { SelectPriceComponent } from './select-price/select-price.component';
import { CreateNormalUserPipe } from './create-normal-user.pipe';


defineCustomElements(window);
@NgModule({
  declarations: [AppComponent, ProductModalComponent, SelectPriceComponent, CreateNormalUserPipe],
  entryComponents: [ProductModalComponent, SelectPriceComponent],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    AngularFireAuthModule,
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule.enablePersistence(), // to enable offline persistance
    AngularFireDatabaseModule,
    AngularFireStorageModule,
    ColorSketchModule,
    AngularFirestoreModule,
    AngularFireMessagingModule,
    HttpClientModule,
    NgxEditorModule,
    AngularEditorModule,
    HttpClientModule,
    HttpModule,
    FormsModule,
    ReactiveFormsModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [
    Camera,
    WebcamModule,
    StatusBar,
    SplashScreen,
    CallNumber,
    Title,
    Contacts,
    CallLog,
    AuthenticateService,
    Pedometer,
    BarcodeScanner,
    Geolocation,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    Diagnostic
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
