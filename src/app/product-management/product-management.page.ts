import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-product-management',
  templateUrl: './product-management.page.html',
  styleUrls: ['./product-management.page.scss'],
})
export class ProductManagementPage implements OnInit {
  products = [];
  constructor(public router: Router, public productService: ProductService, ) { }

  ngOnInit() {
    this.productService.getProducts().subscribe(resp => {
      this.products = [];
      resp.map((data: any) => {
        this.products.push(data);
      });
      this.products.sort((a, b) => parseInt(a.orderby) - parseInt(b.orderby));
    });
  }

  goToFeature(item) {
    this.router.navigate(['add-feature', item.productDocId]);
  }

  goToProduct() {
    this.router.navigate(['/add-product']);
  }


  goToHowItWork(item) {
    this.router.navigate(['/add-how-it-works', item.productDocId]);
  }

  editProduct(productId) {
    this.router.navigate(['edit-product', productId]);
  }

  goToPricing(item) {
    this.router.navigate(['add-price', item.productDocId]);
  }


  manageCover(item) {
    this.router.navigate(['add-product-cover', item.productDocId]);
  }

  deleteProduct(productId) {
    this.productService.deleteProduct(productId).then(resp => {

    });
  }

}
