import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProductManagementPageRoutingModule } from './product-management-routing.module';

import { ProductManagementPage } from './product-management.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    ProductManagementPageRoutingModule
  ],
  declarations: [ProductManagementPage]
})
export class ProductManagementPageModule { }
