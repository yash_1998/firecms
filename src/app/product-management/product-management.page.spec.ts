import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProductManagementPage } from './product-management.page';

describe('ProductManagementPage', () => {
  let component: ProductManagementPage;
  let fixture: ComponentFixture<ProductManagementPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductManagementPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProductManagementPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
