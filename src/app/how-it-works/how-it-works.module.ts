import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { HowItWorksPageRoutingModule } from './how-it-works-routing.module';

import { HowItWorksPage } from './how-it-works.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentsModule,
    HowItWorksPageRoutingModule
  ],
  declarations: [HowItWorksPage]
})
export class HowItWorksPageModule { }
