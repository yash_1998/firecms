import { Component, OnInit } from '@angular/core';
import { ProductService } from '../service/product.service';
import { CommonService } from '../common.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-how-it-works',
  templateUrl: './how-it-works.page.html',
  styleUrls: ['./how-it-works.page.scss'],
})
export class HowItWorksPage implements OnInit {
  prouductId: any;
  index: any;
  selectedProduct = {} as any;
  constructor(
    public router: Router, public productService: ProductService,
    public commonService: CommonService, private route: ActivatedRoute
  ) { }

  ngOnInit() {

  }

}
