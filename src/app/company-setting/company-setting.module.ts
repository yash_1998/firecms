import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ColorSketchModule } from 'ngx-color/sketch';
import { IonicModule } from '@ionic/angular';

import { CompanySettingPageRoutingModule } from './company-setting-routing.module';

import { CompanySettingPage } from './company-setting.page';
import { ComponentsModule } from '../components/components.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentsModule,
    IonicModule,
    ColorSketchModule,
    CompanySettingPageRoutingModule
  ],
  declarations: [CompanySettingPage]
})
export class CompanySettingPageModule { }
