import { Component, OnInit } from '@angular/core';
import { CommonService } from '../common.service';
import { ActivatedRoute, NavigationStart, Router } from '@angular/router';
import { PolicyService } from '../service/policy.service';
import { UploadService } from '../service/upload.service';

@Component({
  selector: 'app-company-setting',
  templateUrl: './company-setting.page.html',
  styleUrls: ['./company-setting.page.scss'],
})
export class CompanySettingPage implements OnInit {
  logoInfo = {} as any;
  constructor(
    public policyService: PolicyService, public commonService: CommonService,
    public route: ActivatedRoute, public router: Router, public uploadService: UploadService) { }

  ngOnInit() {
    this.loadColor();
    this.policyService.getExtraInfoId('primaryColor').subscribe((resp: any) => {
      if (resp) {
        this.commonService.primaryColor = resp.color;
      }
    })
    this.policyService.getExtraInfoId('companylogo').subscribe(response => {
      this.logoInfo = response;
      console.log(response);
    });
    this.router.events.subscribe((event: NavigationStart) => {
      if (event instanceof NavigationStart) {
        this.loadColor();
        // this.segmentChanged(null, this.selectedSegment);
      }
    });
  }

  loadColor() {
    // this.commonService.getObject('primaryColor').then(resp => {
    //   if (resp) {
    //     this.commonService.primaryColor = resp.hex;
    //   } else {
    //     this.commonService.primaryColor = '#8c8cf9';
    //     this.commonService.setObject('primaryColor', { hex: '#8c8cf9' });
    //   }
    // }).catch(error => {
    //   this.commonService.primaryColor = '#8c8cf9';
    //   this.commonService.setObject('primaryColor', { hex: '#8c8cf9' });
    // });

  }



  changeComplete(event) {
    if (event && event.color && event.color.hex) {
      this.commonService.primaryColor = event.color.hex;
      this.policyService.setExtraInfo('primaryColor', { id: 'primaryColor', color: event.color.hex });
    }
  }

  createGroup(data) {
    this.policyService.createGroup(data.group_url + data.group_id, data).subscribe(resp => {
      console.log(resp, 'resp');
    })
  }

  goToHowItWork() {
    this.router.navigate(['/add-how-it-works']);
  }



  imageUpload(event) {
    if (event.target.files[0]) {
      const file: File = event.target.files[0];
      this.uploadService.ImageUpload(file, ('Images/logos/')).then(resp => {
        this.policyService.setExtraInfo('companylogo', { url: resp, id: 'companylogo' }).then(set => {
          console.log(set);
        });
      }).catch(error => {
        console.log(error);
      });

    } else {
      // this.alertService.dismissLoader();
    }
  }

}
