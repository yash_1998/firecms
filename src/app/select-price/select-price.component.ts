import { Component, OnInit, Input } from '@angular/core';
import { ProductService } from '../service/product.service';
import { PolicyService } from '../service/policy.service';
import { CommonService } from '../common.service';
import * as moment from 'moment';
import { Storage } from '@ionic/storage';
import { CrudService } from '../service/crud.service';
import { ModalController } from '@ionic/angular';
declare var Razorpay;
@Component({
  selector: 'app-select-price',
  templateUrl: './select-price.component.html',
  styleUrls: ['./select-price.component.scss'],
})
export class SelectPriceComponent implements OnInit {
  @Input() productInfo;
  @Input() userData;
  myProducts: any[];
  filterPrice = [];
  ipData = {} as any;
  siteType = '';
  selectedPrice = 'monthly';
  constructor(public modalController: ModalController, public crudService: CrudService, public commonService: CommonService, private storage: Storage, public productService: ProductService, public policyService: PolicyService) { }

  ngOnInit() {
    if (window.location.origin) {
      const findIndex = window.location.origin.search('localhost');
      if (findIndex >= 0) {
        this.siteType = 'localhost';
      } else {
        this.siteType = 'original';
      }
    }
    this.storage.get('loggedInUser').then((val) => {
      if (val) {
        this.crudService.getActiveProduct().subscribe(resp => {
          this.myProducts = [];
          resp.forEach((actions) => {
            const body: any = actions.payload.doc.data();
            if (body.userId === val) {
              this.myProducts.push(body);
            }
          });
        });
        this.crudService.getUserData(val).then(userData => {
          console.log(userData);
          userData.forEach(doc => {
            this.userData = doc.data();
            console.log(doc.data());
          });
        })
      }
    });
    console.log(this.productInfo);
    this.filterPriceData();
  }

  segmentChanged(event) {
    this.selectedPrice = event.target.value;
    this.filterPriceData();
  }



  filterPriceData() {
    this.commonService.presentIndicator().then(loader => {
      if (this.productInfo.prices) {
        this.filterPrice = [...this.productInfo.prices];
      }

      this.productService.getLocationDetails().subscribe(resp => {
        let filterData = [];
        this.ipData = resp.data;
        if (this.ipData.country !== 'IN') {
          filterData = this.filterPrice.filter(item => {
            return item.currency === 'USD' && this.selectedPrice === item.plan.period
          });
        } else {
          filterData = this.filterPrice.filter(item => {
            return item.currency === 'INR' && this.selectedPrice === item.plan.period
          });
        }
        console.log(filterData, 'filter data');
        this.filterPrice = [...filterData];
        this.commonService.dismissIndicator();
      }, () => {
        this.commonService.dismissIndicator();
      })
    });
  }



  buyNow(item, i) {
    this.commonService.presentIndicator();
    if (item.stripe_price_id) {
      if (item.plan && item.plan.period !== 'one_time') {
        this.productService.createStripeSessions({ prev_subscription_id: this.productInfo.subscription_id, siteType: this.siteType, index: i, product_id: this.productInfo.productDocId, stripe_customer_id: this.userData.stripe_customer_id, stripe_price_id: item.stripe_price_id }).subscribe(resp => {
          console.log(resp)
          const parseData = JSON.parse(resp.data);
          console.log(parseData, 'parseData');
          if (parseData.url) {
            window.open(parseData.url, '_self');
          }
        }, (e) => {
          const parseData = JSON.parse(e.data);
          console.log(parseData);
        });
      } else {
        this.productService.createStripeOneTimeSessions({ siteType: this.siteType, index: i, product_id: this.productInfo.productDocId, stripe_customer_id: this.userData.stripe_customer_id, stripe_price_id: item.stripe_price_id }).subscribe(resp => {
          console.log(resp)
          this.commonService.dismissIndicator();
          const parseData = resp.data;
          console.log(parseData, 'parseData');
          if (parseData.url) {
            window.open(parseData.url, '_self');
          }
        }, (e) => {
          const parseData = JSON.parse(e.data);
          console.log(parseData);
        });
      }


    } else {
      if (item.plan && item.plan.period !== 'one_time') {
        this.productService.createSubscribtions(item).subscribe(resp => {
          this.commonService.dismissIndicator();
          try {
            const parseData = JSON.parse(resp.data);
            console.log(parseData);
            console.log(parseData.id);
            this.razorpayMethod(parseData, item);
          } catch (e) {
          }
          console.log(resp);
        });
      } else {
        this.razorpayMethod({}, item);
      }
    }
  }


  razorpayMethod(parseData, item) {
    const options = {
      key: 'rzp_live_qAGcCTrXcDTcJQ', // Enter the Key ID generated from the Dashboard
      name: 'Officeplus',
      subscription_id: parseData.id ? parseData.id : null,
      currency: 'INR',
      description: 'Test Transaction',
      external: {
        wallets: ['paytm', 'amazonpay', 'citrus'],
        handler: (data) => {
          console.log(data);
        }
      },
      image: 'https://firebasestorage.googleapis.com/v0/b/digilyceum-15c27.appspot.com/o/UploadedImages%2Frazorpay.svg?alt=media&token=2c5957bb-dcfe-4759-aa53-6d145b361116',
      handler: (response) => {
        console.log(response);
        if (response.razorpay_payment_id) {
          this.commonService.presentIndicator();
          if (this.productInfo.subscription_id) {
            this.productService.cancelRazorPaySubscriptions({ prev_subscription_id: this.productInfo.subscription_id }).subscribe(data => {
              console.log(data);
            });
          }
          if (parseData.id) {
            this.productService.getRazorpaySubscribtionsById({ subscription_id: parseData.id }).subscribe(resp => {
              item.end_date = moment(resp.data.current_end * 1000).add(14, 'd').format();
              this.changeUserStatus(response, { subscription_id: parseData.id, ...item });
            });
          } else {
            item.end_date = moment().add(100, 'y').format();
            this.commonService.dismissIndicator();
            this.changeUserStatus(response, { ...item });
          }

        }
        // alert(response.razorpay_payment_id);
        // alert(response.razorpay_order_id);
        // alert(response.razorpay_signature);
      },
      prefill: {
        // name: 'Gaurav Kumar',
        // email: 'gaurav.kumar@example.com',
        // contact: '9999999999'
      },
      notes: {
        address: 'Razorpay Corporate Office'
      },
      theme: {
        color: '#F37254'
      }
    } as any;
    if (!parseData.id) {
      delete options.subscription_id;
      options.amount = item.price * 100;
    }
    console.log(options)
    const rzp1 = new Razorpay(options);
    rzp1.open();

  }



  changeUserStatus(response, item) {
    if (response.subscription_id || item.subscription_id) {
      this.crudService.showToast(`your transaction is successfully your subscription started for ${this.productInfo.product_name}`);
    } else {
      this.crudService.showToast(`your transaction is successfully for ${this.productInfo.product_name}`);
    }
    const findIndex = this.myProducts.findIndex((item) => item.productDocId === this.productInfo.productDocId);
    let body = {} as any;
    if (findIndex > -1) {
      body = { ...this.myProducts[findIndex] }
    }
    body.paid_user = true;
    if (response.razorpay_payment_id) {
      body.razorpay_payment_id = response.razorpay_payment_id;
    }
    if (response.session_id) {
      body.session_id = response.session_id;
    }
    if (item.end_date) {
      body.end_date = item.end_date;
    }
    if (response.subscription_id) {
      body.subscription_id = response.subscription_id;
    }
    if (item.subscription_id) {
      body.subscription_id = item.subscription_id;
    }
    if (this.productInfo.key) {
      body.key = this.productInfo.key;
    }
    body.time = moment().format();
    body.plan = item.plan;
    body.price = item.price;
    if (item.no_user) {
      body.no_user = item.no_user;
    }

    body.productDocId = this.productInfo.productDocId;
    body.product_name = this.productInfo.product_name;
    body.product_image = this.productInfo.product_image;
    body.prices = this.productInfo.prices;
    body.features = this.productInfo.features ? this.productInfo.features : '';
    body.userId = this.userData.authId;
    body.product_description = this.productInfo.product_description;
    if (body.activeProductDocId) {
      this.crudService.updateActiveProducts(body.activeProductDocId, body).then(resp => {

      });
    } else {
      this.crudService.manageActiveProduct(this.productInfo.productDocId, this.userData.authId).then(resp => {
        if (resp) {
          this.crudService.addActiveProduct(body).then(resp => {
          });
        }
      });
    }
    this.productService.setProductUser(this.productInfo.productDocId, this.userData.authId, this.productBody(item, response)).then(resp => {
      // this.getProductData();
      this.productService.products$.next('change');
      this.modalController.dismiss('success');
    })
  }



  productBody(item, response) {
    const body = {
      userId: this.userData.authId,
      paid_user: true,
      plan: item.plan,
      start_date: moment().format(),
      no_user: item.no_user ? item.no_user : ''
    } as any;
    if (item.plan_id) {
      body.plan_id = item.plan_id
    }
    if (response.razorpay_payment_id) {
      body.razorpay_payment_id = response.razorpay_payment_id;
    }
    if (item.subscription_id) {
      body.subscription_id = item.subscription_id;
    }
    if (item.end_date) {
      body.end_date = item.end_date;
    }
    if (response.subscription_id) {
      body.subscription_id = response.subscription_id;
    }
    if (response.session_id) {
      body.session_id = response.session_id;
    }
    return body;
  }


  getDateFormat(data) {
    if (data.end_date) {
      return moment(data.end_date).format('DD/MM/YYYY');
    }
  }



}
