import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { AddTitlePage } from './add-title.page';

describe('AddTitlePage', () => {
  let component: AddTitlePage;
  let fixture: ComponentFixture<AddTitlePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddTitlePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(AddTitlePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
