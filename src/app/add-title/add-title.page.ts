import { Component, OnInit } from '@angular/core';
import { PolicyService } from '../service/policy.service';
import { CommonService } from '../common.service';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-add-title',
  templateUrl: './add-title.page.html',
  styleUrls: ['./add-title.page.scss'],
})
export class AddTitlePage implements OnInit {
  browserObject = {} as any;
  constructor(public policyService: PolicyService, public commonService: CommonService, public titleService: Title) { }

  ngOnInit() {
    this.policyService.getExtraInfoId('browser_title').subscribe((resp: any) => {
      console.log(resp);
      this.browserObject = resp;
      if (resp.browser_title) {
        this.titleService.setTitle(resp.browser_title);
      }
    });
  }


  setTitle() {
    this.policyService.setExtraInfo('browser_title', { id: 'browser_title', ... this.browserObject }).then(resp => {
      this.commonService.showAlert('Browser title set');
    });
  }

}
